%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(genpath('toolbox'));

pth_data = ['..' filesep 'data' filesep 'train'];
pth_out = ['..' filesep 'data' filesep 'train_raw'];

% Define classes and max number of samples in each of them
classes = {'carAlong' Inf;...
    'groupRunningAcross' Inf; ...
    'groupRunningAlong' Inf; ...
    'groupWalkAcross' Inf; ...
    'groupWalkAlong' Inf; ...
    'hammer' Inf; ...
    'individualRunningAcross' Inf; ...
    'individualRunningAlong' Inf; ...
    'individualWalkAcross' Inf; ...
    'individualWalkAlong' Inf; ...
    'neutralArmy' Inf};

% Random number generator
rng(1);

%% Randomly select maximum number of samples in each class
dir_mat = [];

for i = 1:size(classes,1) % Over all classes
    dir_tmp = dir([pth_data filesep classes{i,1} '_*.mat']);
    max_samples = classes{i,2};
    
    if((max_samples < Inf) && (max_samples < size(dir_tmp,1)))
        sel = randperm(size(dir_tmp,1));
        sel = sel(1:max_samples);

        dir_tmp = dir_tmp(sel);
    end
    
    disp(['Including ' num2str(size(dir_tmp,1)) ' filese with label ' ...
        classes{i,1}]);
    dir_mat = [dir_mat; dir_tmp];
end

%% Process the signals
res_list = {};

for i = 1:size(dir_mat,1) % Over all *.mat files
    disp(['Processing ' dir_mat(i).name ' (' num2str(i) '/' ...
        num2str(size(dir_mat,1)) ')']);
    
    % Load the *.mat file
    pth_mat = [pth_data filesep dir_mat(i).name];
    [filepath,name] = fileparts(pth_mat);
    pth_csv = fullfile(pth_out,[name '.csv']);
    
%     if(exist(pth_csv,'file'))
%         continue;
%     end
    
    frame = struct([]);
    load(pth_mat);
    
    y = int16(frame.data);
    fs = frame.fs;
    
    % Save the *.csv file
    disp(['Storing the table with results to ' pth_csv]);
    dlmwrite(pth_csv, y.');
end