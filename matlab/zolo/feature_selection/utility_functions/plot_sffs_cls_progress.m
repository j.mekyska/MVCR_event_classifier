function plot_sffs_cls_progress(var)

% plot_sffs_cls_progress(var)
%
% This function plots the variable changes during the sequential floating 
% forward selection algorithm. It prints the evolution of classification
% accuracy, sensitivity and specificity and also selected score function
% (default: 'tss' - tradeoff between sensitivity and specificity). It is
% designed to also plot the number of selected variables changes during
% the running time of the SFFS algorithm.
%
% var       - input structure that holds the change of output variables
%             in SFFS during the function runtime (acc, sen, spe, score)
%
%
%
% --
% ing. Zolt�n Gal�
% xgalaz00@stud.feec.vutbr.cz       
% 
% Department of Telecommunications
% Faculty of Electrical Engineering and Communication
% Brno University of Technology

%% Paths and variables
acc = var.accuracy;
sen = var.sensitivity;
spe = var.specificity;
scr = var.score;

acc_std = var.accuracy_std;
sen_std = var.sensitivity_std;
spe_std = var.specificity_std;
scr_std = var.score_std;

num_f = var.num_features;
num_f = +num_f + 1;

%% Determine the feature set changes
feat_changes = find((abs([num_f 0] - [0 num_f])) == 1);
feat_changes = feat_changes(2:end);

%% Find maximums and minimums
max_score = max(max(scr));
max_index = find(scr == max_score);
max_index = max_index(1);

%% Set the markers
ymark_acc = acc(max_index);
ymark_sen = sen(max_index);
ymark_spe = spe(max_index);

%% Edit score to fit to graph
scr = scr/(max_score);
scr = scr*100;
ymark_score = scr(max_index);

MIN = [min(min(acc)); min(min(sen)); min(min(spe)); min(min(scr))];
MIN = min(MIN);   

MAX = [max(max(acc)); max(max(sen)); max(max(spe)); max(max(scr))];
MAX = max(MAX);

%% Plot SFFS progress during running time
errorbar(acc, acc_std, 'b', 'linewidth', 1.2); hold on; grid on;
errorbar(sen, sen_std, 'r', 'linewidth', 1.2);
errorbar(spe, spe_std, 'g', 'linewidth', 1.2); 
plot(scr, 'k', 'linewidth', 1.5); 
% plot(max_index, ymark_acc,   'b*', 'MarkerSize', 10);
% plot(max_index, ymark_sen,   'r*', 'MarkerSize', 10);
% plot(max_index, ymark_spe,   'g*', 'MarkerSize', 10);
plot(max_index, ymark_score, 'k*', 'MarkerSize', 10);

str = ['' num2str(num_f(1))];
text(1, MIN, str, 'fontweight', 'bold');

if (length(feat_changes) > 2)
    for i = 1:length(feat_changes)
        str = [' ' num2str(num_f(feat_changes(i)))];
        text(feat_changes(i), MIN, str, 'fontweight', 'bold');
        plot([feat_changes(i), feat_changes(i)], ...
            [(MIN - 3.5) (MAX + 3.5)], 'k-', 'linewidth', 1); 
    end
end

hold off;
axis([0 length(scr) (MIN - 3.5) (MAX + 3.5)]);
xlabel('\rightarrow {\it number of changes in (best) feature set} [-]');
ylabel('\rightarrow {\it acc, sen, spe, scr} [%]');
legend('acc', 'sen', 'spe', 'scr', 'Location', 'northwest');