function labels = conv_mat2labels(in_nums, unq_vals)

% out_nums = conv_labels2mat(labels)
% 
% The function converts numbers stored in the input column array to labels
% according to the unique values set as the input parameter.
% 
% in_nums   - input column vector of numerical representation of labels
% unq_vals  - unique values (cell / char) to convert labels into
% labels    - output cell array (column vector) with labels
%
%
%
% --
% ing. Zolt�n Gal�
% xgalaz00@stud.feec.vutbr.cz       
% 
% Department of Telecommunications
% Faculty of Electrical Engineering and Communication
% Brno University of Technology

%% Create output labels cell array
labels = cell(length(in_nums), 1);

%% Convert numbers to labels 
for lab = 1:length(in_nums)
    idx = in_nums(lab);
    
    labels{lab} = unq_vals{idx};
end