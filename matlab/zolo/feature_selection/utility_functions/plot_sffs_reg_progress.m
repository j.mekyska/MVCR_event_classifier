function plot_sffs_reg_progress(var)

% plot_sffs_reg_progress(var)
%
% This function plots the variable changes during the sequential floating 
% forward selection algorithm. It prints the evolution: root mean squared
% and also selected score function (default: 'mae' - mean absolute error)
% It is designed to also plot the number of selected variables changes 
% during the running time of the SFFS algorithm.
%
% var       - input structure that holds the change of output variables
%             in SFFS during the function runtime (rmse, score)
%
%
%
% --
% ing. Zolt�n Gal�
% xgalaz00@stud.feec.vutbr.cz       
% 
% Department of Telecommunications
% Faculty of Electrical Engineering and Communication
% Brno University of Technology

%% Paths and variables
rmse = var.rmse;
scr  = var.score;

rmse_std = var.rmse_std;
scr_std  = var.score_std;

num_f = var.num_features;
num_f = +num_f + 1;

%% Determine the feature set changes
feat_changes = find((abs([num_f 0] - [0 num_f])) == 1);
feat_changes = feat_changes(2:end);

%% Find maximums and minimums
max_score = max(max(scr));
max_index = find(scr == max_score);
max_index = max_index(1);

%% Set the markers
ymark_rmse  = rmse(max_index);
ymark_score = scr(max_index);

MIN = [min(min(rmse)); min(min(scr))];
MIN = min(MIN);   

MAX = [max(max(rmse)); max(max(scr))];
MAX = max(MAX);

%% Plot SFFS progress during running time
errorbar(rmse, rmse_std, 'b', 'linewidth', 1.4); hold on; grid on; 
plot(scr,  'r', 'linewidth', 1.5); 
% plot(max_index, ymark_rmse,  'b*', 'MarkerSize', 10);
plot(max_index, ymark_score, 'r*', 'MarkerSize', 10);

str = ['' num2str(num_f(1))];
text(1, MIN, str, 'fontweight', 'bold');

if (length(feat_changes) > 2)
    for i = 1:length(feat_changes)
        str = [' ' num2str(num_f(feat_changes(i)))];
        text(feat_changes(i), MIN, str, 'fontweight', 'bold');
        plot([feat_changes(i), feat_changes(i)], ...
            [(MIN - 3.5) (MAX + 3.5)], 'k-', 'linewidth', 1); 
    end
end

hold off;
axis([0 length(scr) (MIN - 3.5) (MAX + 3.5)]);
xlabel('\rightarrow {\it number of changes in (best) feature set} [-]');
ylabel('\rightarrow {\it rmse, scr}');
legend('rmse', 'scr', 'Location', 'northwest');