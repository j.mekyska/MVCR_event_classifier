function out_nums = conv_labels2mat(labels)

% out_nums = conv_labels2mat(labels)
% 
% The function converts labels stored in a cell array to numbers according
% to their unique values.
% 
% labels    - input cell array (column vector) with labels (chars / cell)
% out_nums  - converted output numerical representation of input labels
%
%
%
% --
% ing. Zolt�n Gal�
% xgalaz00@stud.feec.vutbr.cz       
% 
% Department of Telecommunications
% Faculty of Electrical Engineering and Communication
% Brno University of Technology

%% Convert labels to numerical representation
if (iscell(labels))
    
    % If labels are stored in cell array, create output array and find
    % unique values of labels to create a dictionary of labels to work
    % with in a process of conversion
    out_nums = zeros(length(labels), 1);
    
    % Iterate trough all labels in the input cell array and find out 
    % the type of data in all cells. If it is a cell array or string
    % search the dictionary to find the numerical representation
    for lab = 1:length(labels)
        if (ischar(labels{lab}) || iscell(labels{lab}))
            unq_vals = unique(labels);
            
            % Convert the input labels to numerical representation
            for unq = 1:length(unq_vals)
                if (strcmp(labels{lab}, unq_vals{unq}))
                    out_nums(lab) = unq;
                end
            end
        else
            out_nums(lab) = cell2mat(labels(lab));
        end
    end
else
    error('The input data (labels) must be stored in cell array');
end