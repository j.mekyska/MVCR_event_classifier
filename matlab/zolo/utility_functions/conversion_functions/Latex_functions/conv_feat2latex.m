function [out] = conv_feat2latex(in, h_notation, l_notation)

% [out] = conv_feat2latex(in, h_notation, l_notation)
%
% This function converts speech feature names (long notation) -> full name 
% (local level + high level) into latex notation (short notation), that is
% used for feature representation in tables or graphs
%
% in            - input cell array of feature name strings 
% h_notation    - input path (full) to high level feature notations *.xlsx
% l_notation    - input path (full) to high level feature notations *.xlsx
% out           - output cell array of feature names (latex notation)
%
% EXAMPLE:
% h_notation = 'high_level_features_notation.xlsx';
% l_notation = 'local_level_features_notation.xlsx';
% l_features = {                                                   ...
%    {'offset of linear regression of harmonic-to-Noise ratio'};   ...
%    {'interquartile range of fluctuation amplitudes of DFA'};     ...
%    {'10th percentile of glottal-to-noise excitation ratio'};     ...
%    {'kurtosis of normalized noise energy'};                      ...
%    {'median of TKEO of fundamental frequency'};                  ...
%    {'modulation of glottis quotient open'};                      ...
%    {'studentized range of zero-crossing rate'}};
% out = conv_feat2latex(l_features, ...
%                       h_notation, ...
%                       l_notation);

%% Variables and constants
if (nargin < 3) 
    l_notation = 'local_level_features_notation.xlsx';
end
if (nargin < 2) 
    h_notation = 'high_level_features_notation.xlsx';
end

%% Load high level features notation
[~, ~, h_feat_tbl] = xlsread(h_notation, 'notation');

num_h_feat = size(h_feat_tbl, 1) - 1;
mtx_h_feat = h_feat_tbl(2:end, :);

% mtx_h_feat description: (cell array structure)
% HIGH LEVEL FEATURES
%
% | 1. col | 2. col | 3. col  |   4. col   |      5. col     |
% |  name  | descr. | abbrev. | latex not. | add. latex not. |
%     .         .        .           .              .
%     .         .        .           .              .
% ... num_h_feat
%
% - name             - feature name (full)
% - descr.           - feature description (full)
% - abbrev.          - feature abbreviation
% - latex not.       - feature latex notation
% - add. latex. not. - additional latex notation

H.name  = 1;
H.desc  = 2;
H.abbr  = 3;
H.l_not = 4;
H.addit = 5;

%% Load local level features notation
[~, ~, l_feat_tbl] = xlsread(l_notation, 'notation');

num_l_feat = size(l_feat_tbl, 1) - 1;
mtx_l_feat = l_feat_tbl(2:end, :);

% mtx_l_feat description: (cell array structure)
% LOCAL LEVEL FEATURES
%
% | 1. col | 2. col | 3. col  |   4. col   |      5. col     |   6. col  |
% |  name  | descr. | abbrev. | latex not. | add. latex not. | is vector |
%     .         .        .           .              .              .
%     .         .        .           .              .              .
% ... num_l_feat
%
% - name             - feature name (full)
% - descr.           - feature description (full)
% - abbrev.          - feature abbreviation
% - latex not.       - feature latex notation
% - add. latex. not. - additional latex notation
% - is vector        - features (vector [1/0]; 1 = vector, 0 = scalar)

L.name  = 1;
L.desc  = 2;
L.abbr  = 3;
L.l_not = 4;
L.addit = 5;
L.vect  = 6;

%% Preprocess input data (if needed)
if (iscell(in))
    [rows, cols] = size(in);
    if (cols > rows)
        in = in(:);
    end 
elseif (ischar(in))
    [rows, cols] = size(in);            
    if (rows > cols)
        in = in.';
    end 
else
    warning(['unsupported input variable type' ...
        'function returns inf']);
    out = inf;
    return;
end 

%% Prepare output cell array
out = cell(size(in, 1), 1);

%% Convert long feature names into latex notation
for feat = 1:size(in, 1);
    
    % ------------------------------------------------------------------
    % Set temporary variable
    if (iscell(in))
        curr = in{feat}; 
    elseif (ischar(in))
        curr = in;
    end 
 
    loc_latex  = '';
    high_latex = '';
    
    % ------------------------------------------------------------------
    % Search for local level features
    LCS_length = 0;
    
    for abbr_loc = 1:num_l_feat
        abbr_act = mtx_l_feat{abbr_loc, L.desc};
        temp_idx = strfind(curr, abbr_act);
        
        if (~isempty(temp_idx))
            [~, tmp_length, ~] = ...
                longest_common_substring(curr, abbr_act);
            
            % Check if found substring is a longest common substring
            if (tmp_length > LCS_length)
                LCS_length = tmp_length;
                loc_idx    = abbr_loc;
                loc_strt   = temp_idx;
                loc_desc   = mtx_l_feat{abbr_loc, L.desc};
                loc_latex  = mtx_l_feat{abbr_loc, L.l_not};
            end
        end 
    end
    
    if (LCS_length == 0)
        warning(['undefined local level feature: ' curr ...
            ' function returns inf']);
        out = inf;
        return;
    end
    
    % ------------------------------------------------------------------
    % Search for high level features
    LCS_length = 0;
    
    for abbr_high = 1:num_h_feat 
        abbr_act  = mtx_h_feat{abbr_high, H.desc};
        temp_idx  = strfind(curr, abbr_act);
        
        if (~isempty(temp_idx))
            [~, tmp_length, ~] = ...
                longest_common_substring(curr, abbr_act);

            % Check if found substring is a longest common substring
            if (tmp_length > LCS_length)
                LCS_length = tmp_length;
                high_desc  = mtx_h_feat{abbr_high, H.desc};
                high_latex = mtx_h_feat{abbr_high, H.l_not};
                high_addit = mtx_h_feat{abbr_high, H.addit};
            end

            if (isnan(high_addit))
                high_addit = '';
            end    
        end
    end
      
    % There is high level feature present (is Vector == 1)
    if (mtx_l_feat{loc_idx, L.vect} == 1)
        
        % --------------------------------------------------------------
        % Search for matrix notations
        loc_string    = curr(loc_strt:(loc_strt + length(loc_desc) - 1));
        loc_length    = length(loc_string);
        high_string   = curr(1:length(high_desc));
        high_length   = length(high_string);
        loc_latex_tmp = loc_latex;

        % Set the matrix notation (if necessary)
        if ((loc_length + high_length) < length(curr))
            mtx_notation = curr((length(high_desc) + 1):loc_strt - 1);
            
            if (strfind(mtx_notation, 'TKEO'))
                mtx_notation = '';
            end
        else
            mtx_notation = '';
        end

        % Set the name with matrix notations
        loc_latex = [loc_latex_tmp(1:length('\mbox{')) ...
            mtx_notation loc_latex_tmp((length('\mbox{') + 1):end)]; 

        % --------------------------------------------------------------
        % Search for TKEO descriptiove statistics
        loc_latex_tmp = loc_latex;
        
        if (strfind(high_desc, 'TKEO'))
            
            % Find a position of closing bracket
            for i = length(loc_latex_tmp):(-1):1
                if (~strcmp(loc_latex_tmp(i), '}'))
                    pos_bracket = i;
                    break;
                end
            end
            
            % Set the name with TKEO desc. stats.
            loc_latex = [loc_latex_tmp(1:length('\mbox{')) high_addit ...
                '(' loc_latex_tmp((length('\mbox{') + 1):pos_bracket) ...
                ')' loc_latex_tmp(pos_bracket + 1:end)];
        end
        
    else
        
        % There is no high level feature present (is Vcetor == 0)
        high_latex = '';
    end
    
    % ------------------------------------------------------------------
    % Create latex notation (name)
    out{feat} = ['$' loc_latex high_latex '$'];
end