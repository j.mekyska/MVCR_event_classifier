function [out] = conv_cell2latex(in, options)

% [out] = conv_cell2latex(in, options)
%
% This function converts cell array data into latex notation. It can work
% with raw numerical data and general strings / speech features. Function
% converts long notation of speech features to short form, using external
% function: conv_feat2latex.
%
% in            - input cell array of feature name strings 
% options       - structure holding optional settings of the algorithm
% out           - output cell array of feature names (latex notation)

%% Variables and constants
if ((nargin < 2) || (isempty(options))) 
    options.wrtie_to_txt = true;
    options.output_name  = 'out_table.txt';
    options.h_notation   = 'high_level_features_notation.xlsx';
    options.l_notation   = 'local_level_features_notation.xlsx';
else
    if (~isfield(options, 'wrtie_to_txt'))
        options.wrtie_to_txt = true;
    end 
    if (~isfield(options, 'output_name'))
        options.output_name = 'out_table.txt';
    end    
    if (~isfield(options, 'h_notation'))
        options.h_notation  = 'high_level_features_notation.xlsx';
    end  
    if (~isfield(options, 'l_notation'))
        options.l_notation  = 'local_level_features_notation.xlsx';
    end  
end

%% Set temporary variables (for: code readability)
OPT = options;

%% Open a *.txt file to write the table into
fileID = fopen(OPT.output_name, 'w');

%% Create outpu cell array
out = cell(size(in, 1), 1);

%% Write the table into the file
for row = 1:size(in, 1)
    sel = '';
    
    for col = 1:size(in, 2)
        dat = in{row, col}; 
        
        % Non-numeric value -> check the properties
        if (~isnumeric(dat))
            
            if (~isnan(dat))
                dat = conv_feat2latex(dat, ...
                    OPT.h_notation, OPT.l_notation);
            else
                dat = '';
            end
            
            % Check if the string is general / speech feature
            if (~iscell(dat))
                if (isinf(dat))
                    dat = in{row, col};
                end
            else
                dat = dat{1, 1};
            end
            
            if col ~= size(in, 2)
                frm = '%s & ';
            else
                frm = '%s \\\\';
            end
            
        % Numerical value -> print in desired format
        else
            if (isnan(dat))
                dat = '';
            end
            
            if col ~= size(in, 2)
                frm = '%.4f & ';
            else
                frm = '%.4f \\\\';
            end 
        end
        
        % OUT: Concat the selected latex line
        sel = [sel sprintf(frm, dat)];
        
        % If: True, Write the data into the output file
        if (OPT.wrtie_to_txt)
            fprintf(fileID, frm, dat); 
        end
    end
    
    % OUT: fill the iterated line into th output cell array
    out{row, 1} = sel;
    
    % If: True, Separate data by end-of-line
    if (OPT.wrtie_to_txt)
        fprintf(fileID, '\r\n'); 
    end
end

%% close a *.txt file after writing is finished
fclose(fileID);