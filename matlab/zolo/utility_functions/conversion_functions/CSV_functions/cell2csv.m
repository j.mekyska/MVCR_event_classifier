function cell2csv(cell_array, pth_csv, delimiter)

% cell2csv(cell_array, pth_csv, delimiter)
% 
% This functions store the cell array to the *.csv file.
% 
% cell_array    - input cell array
% pth_csv       - path to the *.csv file
% delimiter     - delimiter (default: ;)

%% Paths and variables
if ((nargin < 3) || (isempty(delimiter)))
    delimiter = ';';
end

%% Open file
FID = fopen(pth_csv, 'w');

%% Store data to the *.csv file
for i = 1:size(cell_array, 1)
    for j = 1:size(cell_array, 2)
        value = cell_array{i, j};
        
        if (~ischar(value))
            if (isempty(value) || isnan(value) || isinf(value))
                value = '';
            else
                if (isnumeric(value))
                    value = num2str(value);
                end
            end
        end
        
        fprintf(FID, '%s', value);
        
        if (j ~= size(cell_array, 2))
            fprintf(FID, delimiter);
        end
    end
    
    fprintf(FID, '\n');
end

%% Close the file
fclose(FID);