function gender = find_gender(label, selector)

% gender = find_gender(label, selector)
% 
% This function returns a gender of a person with input label (PARCZ001).
% 
% label      - input label string (e. g. P2113_8.1_1)
% selector   - cell array, that specifies susbstring for male and female
%              subjects. Structure:
%                           [        ]['healthy'  ]['disease'  ]
%                           ['male'  ]['substring']['substring']
%                           ['female']['substring']['substring']
%
% gender     - output string with selected gender ('male'/'female')

%% Paths and variables
if ((nargin < 2) || (isempty(selector)))
    selector = cell(3, 3); 
    
    selector{1, 1} = ' ';
    selector{1, 3} = 'healthy';
    selector{1, 3} = 'disease';
    
    selector{2, 1} = 'male';
    selector{2, 2} = 'K2';
    selector{2, 3} = 'P2';
    
    selector{3, 1} = 'female';
    selector{3, 2} = 'K1';
    selector{3, 3} = 'P1';
end

%% Select the person's gender
if ((~isempty(strfind(label, selector{2, 2})))  || ...
    (~isempty(strfind(label, selector{2, 3}))))
    gender = 'male';
elseif ((~isempty(strfind(label, selector{3, 2})))  || ...
        (~isempty(strfind(label, selector{3, 3}))))
    gender = 'female';
else
    error('unspecified gender substring');
end