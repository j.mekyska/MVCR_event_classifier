function [pathol] = find_pathol(label, selector)

% [pat_short, pat_long] = find_pathol(label, selector)
% 
% This function returns a medical state (pathology [short, long] strings) 
% of a person with input label (PARCZ001).
% 
% label      - input label string (e. g. P2113_8.1_1)
% selector   - cell array, that specifies susbstring for medical states
%              of subjects. Structure:
%                           [        ]['healthy'  ]['disease'  ]
%                           ['state' ]['substring']['substring']
%
% pathol     - output cell array with selected medical state strings

%% Paths and variables
if ((nargin < 2) || (isempty(selector)))
    selector = cell(2, 3); 
    
    selector{1, 1} = ' ';
    selector{1, 3} = 'healthy';
    selector{1, 3} = 'disease';
    
    selector{2, 1} = 'state';
    selector{2, 2} = 'K';
    selector{2, 3} = 'P';
end

%% Create output cell array
pathol = cell(1, 2);

%% Specify supported pathologies
pathologies =                                               ...
        {{'healthy'},   {'HC'}, {'Healthy'};                ...
         {'parkinson'}, {'PD'}, {'Parkinson''s disease'}};

num_pat = size(pathologies, 1);

%% Select the person's medical state
if (~isempty(strfind(label, selector{2, 2})))
    pathology = 'healthy';
elseif (~isempty(strfind(label, selector{2, 3})))
    pathology = 'parkinson';
else
    error('unspecified pathol substring');
end

%% Set output variables (pathology [short, long])
for pat = 1:num_pat
    if (strcmp(pathology, pathologies{pat, 1}))
        pathol(1, 1) = pathologies{pat, 2};
        pathol(1, 2) = pathologies{pat, 3};
    end
end