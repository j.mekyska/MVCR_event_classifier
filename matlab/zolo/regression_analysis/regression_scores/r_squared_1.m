function [score] = r_squared_1(actual, predic)
score = sum((predic-mean(actual)).^2)/sum((actual-mean(actual)).^2);