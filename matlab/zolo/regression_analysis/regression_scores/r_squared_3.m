function [score] = r_squared_3(actual, predic)
score = 1 - sum((actual-predic).^2)/sum(actual.^2);