function [score] = r_squared_2(actual, predic)
score = sum(predic.^2)/sum(actual.^2);