function [c_train, c_test, obj] = perf_gbtr(d_train, l_train, ...
    d_test, sett, mdl)

% [c_train, c_test, obj] = perf_gbtr(d_train, l_train, d_test, sett)
% 
% This function performs regression (training; testing) of Gradient Boosted
% Trees algorithm. This function returns predicted classes multi-classes => 
% prediction of 'continuous' values) for both training  and testing set of 
% data. The classifier is  specified by setings (sett) structure.
% 
% INPUT DATA:
% d_train   - input training matrix [columns: features; rows: observations]
% d_test    - input testing matrix  [columns: features; rows: observations]
%             NOTE: size(d_train, 2) must be equal to size(d_test, 2)
% l_train   - column vector with labels of the training observations 
%             NOTE: size(l_train, 1) must be equal to size(d_train, 1)
% sett      - structure with the setting for selected classifier
% mdl       - trained model object (optional): used in the case when the
%             training process is skipped and the function only performs
%             the regression (testing step)
%
% OUTPUT DATA:
% c_train   - the classified training class (predicted labels)
% c_test    - the classified testing class (predicted labels)
% obj       - trained model object
%
% CLASSIFIER OPTIONAL DATA:
% for more information, see: 
%   <https://sites.google.com/site/carlosbecker>
%
% sett.loss = Shrinkage factor (max 1.0)
%      {'squaredloss', 'logloss', 'exploss'} 
%      (default: 'squaredloss')
% sett.shrinkageFactor = Shrinkage factor (max 1.0)
%      {0. ... positive float value} 
%      (default: 0.1)
% sett.subsamplingFactor = Subsampling factor
%      {0. ... positive float value} 
%      (default: 0.2)
% sett.maxTreeDepth = Maximum depth of the trees
%      {uint32(int) ... uint32 of integer value}
%      (default: uint32(2))
% sett.randSeed = Random seed
%      {uint32(int) ... uint32 of integer value}
%      (default: uint32(1))
% sett.numIters = Number of fitting iterations
%      {uint32(int) ... uint32 of integer value}
%      (default: uint32(500))
%
%
%
% --
% ing. Zolt�n Gal�
% xgalaz00@stud.feec.vutbr.cz
% 
% Department of Telecommunications
% Faculty of Electrical Engineering and Communication
% Brno University of Technology

%% Paths and variables
if ((nargin < 4) || (isempty(sett)))
    sett.loss              = 'squaredloss';
    sett.shrinkageFactor   = 0.1;
    sett.subsamplingFactor = 0.2;
    sett.maxTreeDepth      = uint32(2);
    sett.randSeed          = uint32(1);
    sett.numIters          = uint32(500);
else
    if (~isfield(sett, 'loss'))
        sett.loss = 'squaredloss';
    end
    if (~isfield(sett, 'shrinkageFactor'))
        sett.shrinkageFactor = 0.1;
    end
    if (~isfield(sett, 'subsamplingFactor'))
        sett.subsamplingFactor = 0.2;
    end
    if (~isfield(sett, 'maxTreeDepth'))
        sett.maxTreeDepth = uint32(2);
    end
    if (~isfield(sett, 'randSeed'))
        sett.randSeed = uint32(1);
    end
    if (~isfield(sett, 'numIters'))
        sett.numIters = uint32(500);
    end
end

if ((nargin < 5) || isempty(mdl))
    mdl_defined = false;
else
    mdl_defined = true;
end

%% Convert labels (if stored in cell array) into numbers
convert = false;

if (iscell(l_train))
    convert = true;
    unq_val = unique(l_train);
    l_train = conv_labels2mat(l_train);
end

%% Train the classifier
if (~mdl_defined)
    obj = SQBMatrixTrain(single(d_train), l_train, sett.numIters, sett);
else
    obj = mdl;
end

%% Evaluate the classifier
c_train = SQBMatrixPredict(obj, single(d_train));
c_test  = SQBMatrixPredict(obj, single(d_test));

%% Convert labels to proper representation (if needed)
if (convert)
    c_train = conv_mat2labels(c_train, unq_val);
    c_test  = conv_mat2labels(c_test, unq_val);
end