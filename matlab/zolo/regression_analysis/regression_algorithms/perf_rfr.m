function [c_train, c_test, obj] = perf_rfr(d_train, l_train, ...
    d_test, sett, mdl)

% [c_train, c_test] = perf_rfr(d_train, l_train, d_test, sett, mdl)
% 
% This functions performs regression (training and testing) of the Random 
% Forests classifier. The function returns predicted classes for both 
% training and testing set of data. The classifier is specified by 
% setings (sett) structure.
% 
% INPUT DATA:
% d_train   - input training matrix [columns: features; rows: observations]
% d_test    - input testing matrix  [columns: features; rows: observations]
%             NOTE: size(d_train, 2) must be equal to size(d_test, 2)
% l_train   - column vector with labels of the training observations 
%             NOTE: size(l_train, 1) must be equal to size(d_train, 1)
% sett      - structure with the setting for selected classifier
% mdl       - trained model object (optional): used in the case when the
%             training process is skipped and the function only performs
%             the classification (testing step)
%
% OUTPUT DATA:
% c_train   - the classified training class (predicted labels)
% c_test    - the classified testing class (predicted labels)
% obj       - trained model object
% 
% CLASSIFIER OPTIONAL DATA:
% sett.num_trees = number of trees (default: 50)
% sett.prior     = prior probability for the classes
%                  {'Empirical', 'Uniform', []} 
%                  (default: [])
%
%
%
% --
% ing. Zolt�n Gal�
% xgalaz00@stud.feec.vutbr.cz       
% 
% Department of Telecommunications
% Faculty of Electrical Engineering and Communication
% Brno University of Technology

%% Paths and variables
if (~isfield(sett, 'num_trees'))
    sett.num_trees = 50;
end
if (~isfield(sett, 'prior'))
    sett.prior = [];
end
if ((nargin < 5) || isempty(mdl))
    mdl_defined = false;
end

%% Convert labels (if stored in cell array) into numbers
convert = false;

if (iscell(l_train))
    convert = true; 
    unq_val = unique(l_train);
    l_train = conv_labels2mat(l_train);
end

%% Train the classifier
if (~mdl_defined)
    obj = TreeBagger(   ...
        sett.num_trees, ...
        d_train,        ...
        l_train,        ...
        'Method', 'regression');
else
    obj = mdl;
end

%% Evaluate the classifier
c_train = predict(obj, d_train);
c_test  = predict(obj, d_test);

%% Convert labels to proper representation (if needed)
if (convert)
    c_train = conv_mat2labels(c_train, unq_val);
    c_test  = conv_mat2labels(c_test, unq_val);
end