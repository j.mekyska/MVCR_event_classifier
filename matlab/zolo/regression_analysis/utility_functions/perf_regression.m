function [c_train, c_test, obj] = perf_regression(d_train, ...
    l_train, d_test, cls, mdl)

% [c_train, c_test, obj] = perf_regression(d_train, ...
%     l_train, d_test, cls, mdl)
% 
% The functions performs regression (the training and testing of selected)
% algorithm. The function returns predicted classes for both training and
% testing set of data. The classifier is determined by optional structure
% 'classifier' that holds the information about the algorithm used.
% 
% Supported algorithms:
%   - Classification and Regression trees (CART)
%   - Ordinal Regression (OR)
%   - Linear Regression (LINR)
%   - Support Vector Machines Regression (SVMR)
%   - Gaussian Process Regression (GPR)
%   - Ensemble Regression (EMR)
%   - Random Forests Regression (RFR)
%   - Gradient Boosted Trees Regression (GBTR)
%
% INPUT DATA:
% d_train   - input training matrix [columns: features; rows: observations]
% d_test    - input testing matrix  [columns: features; rows: observations]
%             NOTE: size(d_train, 2) must be equal to size(d_test, 2)
% l_train   - column vector with labels of the training observations 
%             NOTE: size(l_train, 1) must be equal to size(d_train, 1)
% cls       - structure with the setting for selected classifier
%             NOTE: .alg field is mandatory (determines the classifier)
%                   possible classifiers: 
%                   - Classification and Regression Trees ('cart')
%                   - Ordinal Regression ('or')
%                   - Linear Regression ('linr')
%                   - Support Vector Machines Regression ('svmr')
%                   - Gaussian Process Regression ('gpr')
%                   - Ensemble Regression ('emr')
%                   - Random Forests Regression ('rfr')
%                   - Gradient Boosted Trees Regression ('gbtr')
% mdl       - trained model object (optional): used in the case when the
%             training process is skipped and the function only performs
%             the regression (testing step)
%
% OUTPUT DATA:
% c_train   - the classified training class (predicted labels)
% c_test    - the classified testing class (predicted labels)
% obj       - trained model object
%
% CLASSIFIER OPTIONAL DATA:
%   1) Classification and Regression trees
%       cls.splitcriterion = Split criterion
%           {'mse'} 
%           (default: 'mse')
%       cls.minleaf = Minimum number of leaf node observations
%           {1 ... positive integer value}
%           (default: 1)
%       cls.minparent = Minimum number of branch node observations
%           {10 ... positive integer value}
%           (default: 10)
%       cls.prune = Pruning flag
%           {'on' 'off'}
%           (default: 'on')
%       cls.prunecriterion = Pruning criterion
%           {'mse'}
%           (default: 'mse)
%
%   2) Ordinal Regression
%       cls.model = Type of model to fit 
%           {'nominal' 'ordinal' 'hierarchical'}
%           (default: 'ordinal')
%       cls.link = Link function 
%           {'logit' 'probit' 'comploglog' 'loglog'}
%           (default: 'logit')
%
%   3) Linear Regression
%       cls.modelspec = Model specification
%           {'constant', 'linear', 'interactions', 'purequadratic', ...
%            'quadratic', 'polyijk'} 
%           (default: 'linear')
%       cls.intercept = Indicator for constant term
%           {'true', 'false'}
%           (default: 'true')
%       cls.robustopts = Indicator of robust fitting type
%           {'on', 'off', string}
%            string: {'andrews', 'bisquare', 'cauchy', 'fair',  ...
%                      'huber', 'logistic', 'ols', 'talwar', 'welsch'}
%           (default (robustopts): 'off')
%           (default (string): 'bisquare')
%
%   4) Support Vector Machine Regression
%       cls.kernel = Kernel function
%           {'linear', 'gaussian', 'rbf', 'polynomial'} 
%           (default: 'linear')
%
%   5) Gaussian Process Regression
%       cls.basis = Explicit basis in the GPR model
%           {'constant', 'none', 'linear', 'pureQuadratic'} 
%           (default: 'constant')
%
%   6) Ensemble Models Regression
%       cls.method = Ensemble-aggregation method
%           {'LSBoost', 'Bag'} 
%           (default: 'LSBoost')
%
%   7) Random Forests Regression
%       cls.num_trees = number of trees 
%           {1 ... positive integer value}
%           (default: 50)
%       cls.prior = prior probability for the classes
%           {'Empirical', 'Uniform', []} 
%           (default: [])
%
%   8) Gradient Boosted Trees Regression
%       cls.loss = Shrinkage factor (max 1.0)
%           {'squaredloss', 'logloss', 'exploss'} 
%           (default: 'squaredloss')
%       cls.shrinkageFactor = Shrinkage factor (max 1.0)
%           {0. ... positive float value} 
%           (default: 0.1)
%       cls.subsamplingFactor = Subsampling factor
%           {0. ... positive float value} 
%           (default: 0.2)
%       cls.maxTreeDepth = Maximum depth of the trees
%           {uint32(int) ... uint32 of integer value}
%           (default: uint32(2))
%       cls.randSeed = Random seed
%           {uint32(int) ... uint32 of integer value}
%           (default: uint32(1))
%       cls.numIters = Number of fitting iterations
%           {uint32(int) ... uint32 of integer value}
%           (default: uint32(500))
%
%
%
% --
% ing. Zolt�n Gal�
% xgalaz00@stud.feec.vutbr.cz       
% 
% Department of Telecommunications
% Faculty of Electrical Engineering and Communication
% Brno University of Technology

%% Paths and variables
if ((nargin < 4) || isempty(cls) || (~isfield(cls, 'alg')))
    error(['The input structure ''cls'' must contain field ''alg'' '   ...
           'that determines the selected regression algorithm.']);
end
if ((nargin < 5) || isempty(mdl))
    mdl = [];
end

%% Lower the algorithm specification (for consistency)
cls.alg = lower(cls.alg);

%% Check the regression algorithm
cls.alg = check_regression_alg(cls.alg);

if (isempty(cls.alg))
    cls.alg = 'cart';
    disp(' ');
    disp('Improper classifier selected. Set to default: Cart');
end

%% Train and classify selected classifier
if (strcmp(cls.alg, 'classificationandregressiontrees')             || ...
    strcmp(cls.alg, 'classification and regression trees')          || ...
    strcmp(cls.alg, 'regressiontrees')                              || ...
    strcmp(cls.alg, 'regression trees')                             || ...
    strcmp(cls.alg, 'cart algorithm')                               || ...
    strcmp(cls.alg, 'cart'))
    
    % Classification and Regression Trees algorithm
    cls = check_regression_sett(cls);
    [c_train, c_test, obj] = ...
        perf_cart(d_train, l_train, d_test, cls, mdl);
    
elseif (strcmp(cls.alg, 'ordinalregression')                        || ...
    strcmp(cls.alg, 'ordinal regression')                           || ...
    strcmp(cls.alg, 'ordinal reg')                                  || ...
    strcmp(cls.alg, 'ord regression')                               || ...
    strcmp(cls.alg, 'or algorithm')                                 || ...
    strcmp(cls.alg, 'or'))
    
    % Ordinal Regression algorithm
    cls = check_regression_sett(cls);
    [c_train, c_test, obj] = ...
        perf_or(d_train, l_train, d_test, cls, mdl);
    
elseif (strcmp(cls.alg, 'linearregression')                         || ...
    strcmp(cls.alg, 'linear regression')                            || ...
    strcmp(cls.alg, 'linear reg')                                   || ...
    strcmp(cls.alg, 'lin regression')                               || ...
    strcmp(cls.alg, 'linr algorithm')                               || ...
    strcmp(cls.alg, 'linr'))
    
    % Linear Regression algorithm
    cls = check_regression_sett(cls);
    [c_train, c_test, obj] = ...
        perf_linr(d_train, l_train, d_test, cls, mdl);

elseif (strcmp(cls.alg, 'supportvectormachinesregression')          || ...
    strcmp(cls.alg, 'support vector machines regression')           || ...
    strcmp(cls.alg, 'support vector machines reg')                  || ...
    strcmp(cls.alg, 'svm regression')                               || ...
    strcmp(cls.alg, 'svmr algorithm')                               || ...
    strcmp(cls.alg, 'svmr'))
    
    % SVM Regression algorithm
    cls = check_regression_sett(cls);
    [c_train, c_test, obj] = ...
        perf_svmr(d_train, l_train, d_test, cls, mdl);
    
elseif (strcmp(cls.alg, 'gaussianprocessregression')                || ...
    strcmp(cls.alg, 'gaussian process regression')                  || ...
    strcmp(cls.alg, 'gaussian process reg')                         || ...
    strcmp(cls.alg, 'gp regression')                                || ...
    strcmp(cls.alg, 'gpr algorithm')                                || ...
    strcmp(cls.alg, 'gpr'))
    
    % Gaussian Process Regression algorithm
    cls = check_regression_sett(cls);
    [c_train, c_test, obj] = ...
        perf_gpr(d_train, l_train, d_test, cls, mdl);

elseif (strcmp(cls.alg, 'ensemblermodelsegression')                 || ...
    strcmp(cls.alg, 'ensemble models regression')                   || ...
    strcmp(cls.alg, 'ensemble models reg')                          || ...
    strcmp(cls.alg, 'en regression')                                || ...
    strcmp(cls.alg, 'emr algorithm')                                || ...
    strcmp(cls.alg, 'emr'))
    
    % Gaussian Process Regression algorithm
    cls = check_regression_sett(cls);
    [c_train, c_test, obj] = ...
        perf_emr(d_train, l_train, d_test, cls, mdl);

elseif (strcmp(cls.alg, 'randomforestsregression')                  || ...
    strcmp(cls.alg, 'random forests regression')                    || ...
    strcmp(cls.alg, 'random forests reg')                           || ...
    strcmp(cls.alg, 'rf regression')                                || ...
    strcmp(cls.alg, 'rfr algorithm')                                || ...
    strcmp(cls.alg, 'rfr'))

    % Random Forests Regression algorithm
    cls = check_regression_sett(cls);
    [c_train, c_test, obj] = ...
        perf_rfr(d_train, l_train, d_test, cls, mdl);

elseif (strcmp(cls.alg, 'gradientboostedtrresregression')           || ...
    strcmp(cls.alg, 'gradient boosted trees regression')            || ...
    strcmp(cls.alg, 'gradient boosted trees reg')                   || ...
    strcmp(cls.alg, 'gbt regression')                               || ...
    strcmp(cls.alg, 'gbtr algorithm')                               || ...
    strcmp(cls.alg, 'gbtr'))

    % Gradient Boosted Trees Regression algorithm
    cls = check_regression_sett(cls);
    [c_train, c_test, obj] = ...
        perf_gbtr(d_train, l_train, d_test, cls, mdl);
    
else        
    error(['Classifier ' cls.alg ' is not supported.']);
end