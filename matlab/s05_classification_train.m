%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(genpath('toolbox'));
addpath(genpath('zolo'));
addpath(genpath('FSToolbox'));

pth_data = ['..' filesep 'meta' filesep 'table_results_12k.csv'];

rng(1);

% Filtering feature selection settings
num_sel = 10;
method = 'mrmr';

% Classifier settings
sett.num_cross_validations = 10;
sett.k_fold = 10;
sett.t = 2;
sett.score = 'mcc';

sett.classifier = struct();
sett.classifier.alg = 'gmm';
sett.classifier.mixt = 1;

%% Load the *.csv
table = importdata(pth_data,',',1);

%% Filter out the neutral events
sel = cell2mat(cellfun(@(x) strcmp(x,'neutral'),table.textdata(:,4), ...
    'UniformOutput',false)) == 0;
table.textdata = table.textdata(sel,:);
table.data = table.data(sel(2:end),:);

%% Prepare the labels
labels = cell2mat(cellfun(@(x) strcmp(x,'train') | strcmp(x,'train2') ...
    | strcmp(x,'cargoTrain'),table.textdata(2:end,4), ...
    'UniformOutput',false));
labels = double(labels);

%% Get feature names and their indexes
features = table.textdata(1,5:end).';
features = [features num2cell(1:length(features)).'];

%% Prepare and clean the data
data = table.data;

% Skip columns with NaN values
sel = sum(isnan(data)) == 0;
data = data(:,sel);
features = features(sel,:);

% Skip columns with Inf values
sel = sum(isinf(data)) == 0;
data = data(:,sel);
features = features(sel,:);

% Skip columns with complex values
% sel = sum(~isreal(data)) == 0;
% data = data(:,sel);
% features = features(sel,:);

% Skip columns with same values
sel = range(data) ~= 0;
data = data(:,sel);
features = features(sel,:);

%% Perform filtering feature selection if necessary
if(~isempty(method))
    if(num_sel > size(data,2))
        num_sel = size(data,2);
    end

    % Discretize the variables
    disc_data = data;

    th1 = mean(disc_data) - std(disc_data);
    th2 = mean(disc_data) + std(disc_data);

    th1 = repmat(th1, size(disc_data,1), 1);
    th2 = repmat(th2, size(disc_data,1), 1);

    mask1 = disc_data < th1;
    mask3 = disc_data > th2;        
    mask2 = (mask1+mask3) == 0;

    disc_data(mask1) = -1;
    disc_data(mask2) = 0;
    disc_data(mask3) = 1;

    % Select features
    sel = feature_selection_filter(disc_data, labels, num_sel, method);
    data = data(:,sel.');
    features = features(sel,:);
end

%% Perform SFFS
[out, var] = SFFS_cls(data, labels, sett);

%% Store the results to the table
features = features(out.S.',:);

table_out = [{'ACC (mean)' out.ACC(end); ...
    'ACC (std)' out.ACC_std(end); ...
    'SEN (mean)' out.SEN(end); ...
    'SEN (std)' out.SEN_std(end); ...
    'SPE (mean)' out.SPE(end); ...
    'SPE (std)' out.SPE_std(end); ...
    'MCC (mean)' out.SCR(end); ...
    'MCC (std)' out.SCR_std(end)};
    features];

%% Store the results to *.xlsx table
if(strcmp(computer, 'PCWIN64'))
    [filepath, name] = fileparts(pth_data);
    pth_out = fullfile(filepath, [name '_SFFS_GMM.xlsx']);

    disp(['Storing the classification results to ' pth_out]);
    
    if(exist(pth_out, 'file'))
        delete(pth_out);
    end

    xlswrite(pth_out, table_out);
end