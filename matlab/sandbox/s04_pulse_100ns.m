%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(['..' filesep 'auxiliary']);
pth_data = (['..' filesep '..' filesep 'data' filesep 'can_be_deleted' ...
    filesep 'pulse_100ns']);

% Limits in space
lim_pos_lo = 400;
lim_pos_up = 1400;

fs = 1;

% FFT window
time_lo = 90;
time_up = 100;
index_train = 660;
index_neutral = 300;

%% Read the data
dir_stm = dir([pth_data filesep '*.stm']);
M = size(dir_stm,1); % Number of STM
% M = 22;

S = 0; % Number of spatial samples
F = 0; % Number of time frames

for i = 1:M % Over all *.stm files
    pth_stm = [pth_data filesep dir_stm(i).name];
    rec = stmopen(pth_stm);
    
    S = rec.NbOfSamples;
    F = F + length(1:fs:rec.NbOfFrames);
end

data = int16(zeros(S,F));
stamps = zeros(F,1);
pos = zeros(S,1);

cnt = 1;

for i = 1:M % Over all *.stm files    
    pth_stm = [pth_data filesep dir_stm(i).name];
    rec = stmopen(pth_stm);
    
    disp([num2str(cnt) ' - start of ' pth_stm]);

    for j = 1:fs:rec.NbOfFrames
        [data(:,cnt), stamps(cnt)] = stmread(rec, j);
        cnt = cnt + 1;
    end
end

data = data.';
stamps = (stamps - min(stamps))/1e6;

%% Visualise the whole part
figure;
imagesc(pos,stamps,data);
colormap jet;
colorbar;
xlabel('position \rightarrow');
ylabel('\leftarrow time [s]');

%% Visualise the selected part
data_prt = data(:,lim_pos_lo:lim_pos_up);
pos_prt = pos(lim_pos_lo:lim_pos_up);

figure;
imagesc(pos_prt,stamps,data_prt);
colormap jet;
colorbar;
xlabel('position \rightarrow');
ylabel('\leftarrow time [s]');

%% Visualise the window containing a train
ind_lo = find(stamps >= time_lo,1,'first');
ind_up = find(stamps >= time_up,1,'first');

data_prt = data(ind_lo:ind_up,index_train);

W = 20*log10(abs(fft(data_prt,1024)./length(data_prt)));
W = W(1:ceil(length(W)/2));
f = linspace(0,rec.FrameRate/2,length(W));

figure;
plot(f,W);
grid on;
axis tight;
xlabel('f [Hz] \rightarrow');
ylabel('|S(f)| [dB]');
title(['Magnitude spectrum of selection (train): time = ' ...
    num2str(time_lo) '-' num2str(time_up) ' s, position: ' num2str(index_train)]);

figure;
specgram(data_prt,2048,rec.FrameRate,hamming(fix(0.025*rec.FrameRate)),...
    fix(0.010*rec.FrameRate));
xlabel('t [s]');
ylabel('f [Hz]');
title('Spectrogram (train)');

%% Visualise the window containing a background
ind_lo = find(stamps >= time_lo,1,'first');
ind_up = find(stamps >= time_up,1,'first');

data_prt = data(ind_lo:ind_up,index_neutral);

W = 20*log10(abs(fft(data_prt,1024)./length(data_prt)));
W = W(1:ceil(length(W)/2));
f = linspace(0,rec.FrameRate/2,length(W));

figure;
plot(f,W);
grid on;
axis tight;
xlabel('f [Hz] \rightarrow');
ylabel('|S(f)| [dB]');
title(['Magnitude spectrum of selection (neutral): time = ' ...
    num2str(time_lo) '-' num2str(time_up) ' s, position: ' num2str(index_neutral)]);

figure;
specgram(data_prt,2048,rec.FrameRate,hamming(fix(0.025*rec.FrameRate)),...
    fix(0.010*rec.FrameRate));
xlabel('t [s]');
ylabel('f [Hz]');
title('Spectrogram (neutral)');