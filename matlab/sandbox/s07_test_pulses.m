%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
pth_data = ['..' filesep '..' filesep 'data' filesep 'can_be_deleted' ...
    filesep 'pulses' filesep 'labelled'];
pth_out_dir = ['..' filesep '..' filesep 'data' filesep ...
    'can_be_deleted' filesep 'pulses' filesep 'img'];

close_fig = 0;

cm_with = 20;
cm_height = 15;

NFFT = 256;

%% Design a filter
Fs = 8000;           % Sampling Frequency
Fstop = 4;           % Stopband Frequency
Fpass = 10;          % Passband Frequency
Astop = 80;          % Stopband Attenuation (dB)
Apass = 1;           % Passband Ripple (dB)
match = 'stopband';  % Band to match exactly

h  = fdesign.highpass(Fstop, Fpass, Astop, Apass, Fs);
Hd = design(h, 'butter', 'MatchExactly', match);

%% Check all data
dir_dir = dir(pth_data);
cnt = 1;

final_spec = zeros(NFFT, size(dir_dir,1)-2);
leg = {};

for i = 3:3%size(dir_dir,1) % Over all folders
    dir_dat = dir([pth_data filesep dir_dir(i).name filesep '*.dat']);
    leg = [leg dir_dir(i).name];
    
    % Magnitude spectra
    spec = [];
    
    for j = 1:1%size(dir_dat, 1) % Over all *.dat files
        pth_dat = [pth_data filesep dir_dir(i).name filesep ...
            dir_dat(j).name];
        
        % Get parameters
        prs = sscanf(dir_dat(j).name, 'L%dP%dT%dW%dR%dF%dH%dG%d.dat');
        
        time = prs(3);
        width = prs(5);
        fs = prs(6);
        
        % Get data label and time stamp
        label = dir_dir(i).name;
        
        % Read the data
        FID = fopen(pth_dat, 'rb');
        data = fread(FID, 'int16');
        fclose(FID);

        % Because first data from Bzenec were not OK, comment in future
        data = data(1:floor(length(data)/width)*width);
        
        data = reshape(data,[],width);
        tosa = 1:size(data,1);
        wosa = 0:width-1;
        
        % Normalise the energy
        data = double(data)./repmat(sqrt(sum(abs(data.')).^2).',1,size(data,2));
        
        % Filter the data by a 4Hz high pass filter in time        
%         data = filter(Hd,data);
        
        % Prepare the cuts
        step = fix(size(data,1)/4);
        ind = (1:3)*step;
        
        for k = 1:size(ind,2) % Over all cuts
            spec = [spec 2*abs(fft(data(ind(k),:),NFFT)/size(data,2)).'];
        end
        
        % Plot the whole frame
        h = figure(cnt);
        imagesc(wosa,tosa,data);
        hold on;
        plot([wosa(1); wosa(end)], [ind; ind], 'r');
        colorbar;
        xlabel('position [samples] \rightarrow');
        ylabel('\leftarrow time [samples]');
        title(['Frame of ''' label ''' (' dir_dat(j).name ')']);
        
        pth_img = [pth_out_dir filesep num2str(cnt,'%03.0f') '_' ...
            label '_frame'];
        set(gcf, 'PaperUnits', 'centimeters', 'PaperPosition', ...
            [0 0 cm_with cm_height], 'PaperSize', [cm_with cm_height]);
        print(['-f' num2str(cnt)], '-dpng', pth_img);
        if(close_fig)
            close(h);
        end
        
        cnt = cnt + 1;
    end
    
    % Calculate the mean spectrum
    final_spec(:,i-2) = mean(spec.').';
end

% % Plot the magnitude spectra
% h = figure(cnt);
% plot(final_spec(1:end/2,:));
% grid on;
% axis tight;
% xlabel('k \rightarrow');
% ylabel('S [k] \rightarrow');
% legend(leg);
% 
% pth_img = [pth_out_dir filesep num2str(cnt,'%03.0f') '_' ...
%     'spectra'];
% set(gcf, 'PaperUnits', 'centimeters', 'PaperPosition', ...
%     [0 0 cm_with cm_height], 'PaperSize', [cm_with cm_height]);
% print(['-f' num2str(cnt)], '-dpng', pth_img);
% if(close_fig)
%     close(h);
% end
% 
% cnt = cnt + 1;