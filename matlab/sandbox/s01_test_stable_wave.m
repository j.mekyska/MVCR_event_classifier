%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
pth_dat = ['..' filesep '..' filesep 'data' filesep 'can_be_deleted' ...
    filesep 'P6500W0100T1543498337911811.dat'];

lim_lo = 1.75e5;
lim_up = 2.25e5;

PCA_n = 2;

%% Read the data
data = csvread(pth_dat);

time = data(:,1);
data = int16(data(:,2:end));

%% Extract just a part
data_prt = data(lim_lo:lim_up,:);

%% Downsample in time
data_prt_down = data_prt(1:500:end,:);

t = repmat(1:size(data_prt_down,1),size(data_prt_down,2),1);
l = repmat((1:size(data_prt_down,2)).',1,size(data_prt_down,1));

[X,Y] = meshgrid(1:size(data_prt_down,1),1:size(data_prt_down,2));

%% Visualise the selected part
figure;
imagesc(data_prt);
colormap jet;
colorbar;
xlabel('position \rightarrow');
ylabel('\leftarrow time [samples]');

figure;
surf(X,Y,data_prt_down.','EdgeColor','none');
grid on;
xlabel('\leftarrow time [samples]');
ylabel('position \rightarrow');
title('Original input');
view(130,70);

%% Perform PCA reconstruction
mu = mean(data_prt_down);
[eigenvectors, scores] = pca(double(data_prt_down));

data_prt_down_rec = scores(:,1:PCA_n) * eigenvectors(:,1:PCA_n)';
data_prt_down_rec = bsxfun(@plus, data_prt_down_rec, mu);

%% Visualise the reconstructed part
figure;
surf(X,Y,data_prt_down_rec.','EdgeColor','none');
grid on;
xlabel('\leftarrow time [samples]');
ylabel('position \rightarrow');
title(['After reconstruction (no. of PCA components: ' num2str(PCA_n) ')']);
view(120,50);