%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(['..' filesep 'auxiliary']);
pth_data = (['..' filesep '..' filesep 'data' filesep 'can_be_deleted' ...
    filesep 'pulse_100ns']);

% Select rows x(start) x(end) y label
limits = {400 500 170 'train';...
    600 700 147 'train'; ...
    900 1000 114 'train'; ...
    1200 1300 33 'train'; ...
    500 600 19 'neutral'; ...
    1000 1100 159 'neutral'; ...
    2100 2300 97 'neutral';
    3000 3100 36 'neutral'};

fs = 8000;

if(fs ~= 1)
    cell2mat(limits(:,3))
end

%% Read the data
dir_stm = dir([pth_data filesep '*.stm']);
M = size(dir_stm,1); % Number of STM

S = 0; % Number of spatial samples
F = 0; % Number of time frames

for i = 1:M % Over all *.stm files
    pth_stm = [pth_data filesep dir_stm(i).name];
    rec = stmopen(pth_stm);
    
    S = rec.NbOfSamples;
    F = F + length(1:fs:rec.NbOfFrames);
end

data = int16(zeros(S,F));
stamps = zeros(F,1);
pos = zeros(S,1);

cnt = 1;

for i = 1:M % Over all *.stm files    
    pth_stm = [pth_data filesep dir_stm(i).name];
    rec = stmopen(pth_stm);
    
    disp([num2str(cnt) ' - start of ' pth_stm]);

    for j = 1:fs:rec.NbOfFrames
        [data(:,cnt), stamps(cnt)] = stmread(rec, j);
        cnt = cnt + 1;
    end
end

data = data.';
stamps = (stamps - min(stamps))/1e6;

%% Visualise the whole part
figure;
imagesc(data);
colormap jet;
colorbar;
xlabel('position \rightarrow');
ylabel('\leftarrow time [s]');
