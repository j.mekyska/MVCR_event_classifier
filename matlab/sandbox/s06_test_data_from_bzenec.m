%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
pth_data = ['..' filesep '..' filesep 'data' filesep 'can_be_deleted' ...
    filesep 'bzenec2' filesep 'labelled'];
pth_out_dir = ['..' filesep '..' filesep 'data' filesep ...
    'can_be_deleted' filesep 'bzenec2' filesep 'img'];

close_fig = 1;

cm_with = 15;
cm_height = 10;

%% Check spectrograms of the data
dir_dir = dir(pth_data);
cnt = 1;

for i = 3:size(dir_dir,1) % Over all data folders
    dir_dat = dir([pth_data filesep dir_dir(i).name filesep '*.dat']);
    
    for j = 1:size(dir_dat,1) % Over all *.dat files
        pth_dat = [pth_data filesep dir_dir(i).name filesep ...
            dir_dat(j).name];
        
        % Get parameters
        prs = sscanf(dir_dat(j).name, 'L%dP%dT%dW%dF%dH%d.dat');
        
        len = prs(1);
        position = prs(2);
        time = prs(3);
        width = prs(4);
        fs = prs(5);
        threshold = prs(6);
        
        % Get data label and time stamp
        stamp = char(datetime(time, 'ConvertFrom', 'posixtime'));
        label = dir_dir(i).name;
        
        switch(dir_dir(i).name)
            case 'approaching-car'
                bw = 700;
            case 'car-on-fibre'
                bw = 500;
            case 'hammer'
                bw = 500;
            case 'jumping'
                bw = 400;
            case 'running-and-crossing-backfill'
                bw = 1000;
            case 'running-not-crossing-backfill'
                bw = 700;
            case 'walk'
                bw = 700;
            case 'walk-recorded-by-the-first-setup'
                bw = 700;
            otherwise
                error(['Unknown event ' dir_dir(i).name]);
        end
        
        % Read the data
        FID = fopen(pth_dat, 'rb');
        data = fread(FID, 'int16');
        fclose(FID);

        % Because first data from Bzenec were not OK, comment in future
        data = data(1:floor(length(data)/width)*width);
        
        data = reshape(data,[],width);
        tosa = (0:size(data,1)-1)/fs;
        wosa = 0:width-1;
        
        % Select only column with the highest energy
        [~, ind] = max(max(abs(data)));
        
        % Plot the whole frame
        h = figure(cnt);
        imagesc(wosa,tosa,data);
        hold on;
        plot([ind-1; ind-1],[tosa(1); tosa(end)],'r');
        xlabel('position [samples] \rightarrow');
        ylabel('\leftarrow time [s]');
        title(['Frame of event ''' label ''' (' stamp ')']);
        
        pth_img = [pth_out_dir filesep num2str(cnt,'%03.0f') '_' ...
            label '_frame'];
        set(gcf, 'PaperUnits', 'centimeters', 'PaperPosition', ...
            [0 0 cm_with cm_height], 'PaperSize', [cm_with cm_height]);
%         print(['-f' num2str(cnt)], '-dpdf', pth_img);
        print(['-f' num2str(cnt)], '-dpng', pth_img);
%         savefig(h,[pth_img '.fig']);
        if(close_fig)
            close(h);
        end
        cnt = cnt + 1;
        
        % Plot the spectrogram
        h = figure(cnt);
        subplot(3,1,1);
        plot(tosa,data(:,ind));
        grid on;
        axis tight;
        xlabel('t [s] \rightarrow');
        ylabel('s(t) [-] \rightarrow')
        title(['Waveform/spectrogram of event ''' label ''' (' stamp ')']);
        
        subplot(3,1,2:3);
        specgram(data(:,ind),8e3,fs,hamming(fix(0.030*fs)),fix(0.010*fs));
        xlabel('t [s] \rightarrow');
        ylabel('f [Hz] \rightarrow');
        
        pth_img = [pth_out_dir filesep num2str(cnt,'%03.0f') '_' ...
            label '_spectrogram'];
        set(gcf, 'PaperUnits', 'centimeters', 'PaperPosition', ...
            [0 0 cm_with cm_height], 'PaperSize', [cm_with cm_height]);
%         print(['-f' num2str(cnt)], '-dpdf', pth_img);
        print(['-f' num2str(cnt)], '-dpng', pth_img);
%         savefig(h,[pth_img '.fig']);
        if(close_fig)
            close(h);
        end
        cnt = cnt + 1;
        
        % Plot just a part of spectrogram
        h = figure(cnt);
        subplot(3,1,1);
        plot(tosa,data(:,ind));
        grid on;
        axis tight;
        xlabel('t [s] \rightarrow');
        ylabel('s(t) [-] \rightarrow')
        title(['Waveform/spectrogram of event ''' label ''' (' stamp ')']);
        
        subplot(3,1,2:3);
        specgram(data(:,ind),8e3,fs,hamming(fix(0.030*fs)),fix(0.010*fs));
        xlabel('t [s] \rightarrow');
        ylabel('f [Hz] \rightarrow');
        axis([tosa(1) tosa(end) 0 bw]);
        
        pth_img = [pth_out_dir filesep num2str(cnt,'%03.0f') '_' ...
            label '_spectrogram_zoom'];
        set(gcf, 'PaperUnits', 'centimeters', 'PaperPosition', ...
            [0 0 cm_with cm_height], 'PaperSize', [cm_with cm_height]);
%         print(['-f' num2str(cnt)], '-dpdf', pth_img);
        print(['-f' num2str(cnt)], '-dpng', pth_img);
%         savefig(h,[pth_img '.fig']);
        if(close_fig)
            close(h);
        end
        cnt = cnt + 1;
    end
end