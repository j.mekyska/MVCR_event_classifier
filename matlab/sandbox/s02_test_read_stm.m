%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(['..' filesep 'auxiliary']);
pth_stm = (['..' filesep '..' filesep 'data' filesep 'raw' filesep ...
    'T1539173041_1.stm']);

%% Get info about the record
rec = stmopen(pth_stm);