%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(['..' filesep 'auxiliary']);
pth_data = (['..' filesep '..' filesep 'data' filesep 'can_be_deleted' ...
    filesep 'wave']);

% Limits in time
lim_lo = 1;
lim_up = 18e3;

% Limits in space
lim_pos_lo = 2300;
lim_pos_up = 2900;

PCA_n = 2;

%% Read the data
dir_stm = dir([pth_data filesep '*.stm']);
M = size(dir_stm,1); % Number of STM

S = 0; % Number of spatial samples
F = 0; % Number of time frames

for i = 1:M % Over all *.stm files
    pth_stm = [pth_data filesep dir_stm(i).name];
    rec = stmopen(pth_stm);
    
    S = rec.NbOfSamples;
    F = F + rec.NbOfFrames;
end

data = int16(zeros(S,F));
cnt = 1;

for i = 1:M % Over all *.stm files    
    pth_stm = [pth_data filesep dir_stm(i).name];
    rec = stmopen(pth_stm);

    for j = 1:rec.NbOfFrames
        data(:,cnt) = stmread(rec, j);
        cnt = cnt + 1;
    end
end

data = data.';

%% Extract just a part
data_prt = data(lim_lo:lim_up,lim_pos_lo:lim_pos_up);

%% Downsample in time
data_prt_down = data_prt(1:500:end,:);

t = repmat(1:size(data_prt_down,1),size(data_prt_down,2),1);
l = repmat((1:size(data_prt_down,2)).',1,size(data_prt_down,1));

[X,Y] = meshgrid(1:size(data_prt_down,1),1:size(data_prt_down,2));

%% Visualise the whole part
figure;
imagesc(data);
colormap jet;
colorbar;
xlabel('position \rightarrow');
ylabel('\leftarrow time [samples]');

%% Visualise the selected part
figure;
imagesc(data_prt);
colormap jet;
colorbar;
xlabel('position \rightarrow');
ylabel('\leftarrow time [samples]');

figure;
surf(X,Y,data_prt_down.','EdgeColor','none');
grid on;
xlabel('\leftarrow time [samples]');
ylabel('position \rightarrow');
title('Original input');
view(130,70);

%% Perform PCA reconstruction
mu = mean(data_prt_down);
[eigenvectors, scores] = pca(double(data_prt_down));

data_prt_down_rec = scores(:,1:PCA_n) * eigenvectors(:,1:PCA_n)';
data_prt_down_rec = bsxfun(@plus, data_prt_down_rec, mu);

%% Visualise the reconstructed part
figure;
surf(X,Y,data_prt_down_rec.','EdgeColor','none');
grid on;
xlabel('\leftarrow time [samples]');
ylabel('position \rightarrow');
title(['After reconstruction (no. of PCA components: ' num2str(PCA_n) ')']);
view(120,50);