%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(['..' filesep 'auxiliary']);
pth_data = (['..' filesep '..' filesep 'data' filesep 'can_be_deleted' ...
    filesep 'pulses' filesep 'raw']);

fs = 1e2;
NFFT = 2048;

%% Process the data
dir_dir = dir(pth_data);

full_spec = [];
leg = {};

for i = 3:size(dir_dir,1) % Over all pulses
    % Read the data
    dir_stm = dir([pth_data filesep dir_dir(i).name filesep '*.stm']);
    leg = [leg dir_dir(i).name];
    
    M = size(dir_stm,1); % Number of STM

    S = 0; % Number of spatial samples
    F = 0; % Number of time frames

    for j = 1:M % Over all *.stm files
        pth_stm = [pth_data filesep dir_dir(i).name filesep dir_stm(j).name];
        rec = stmopen(pth_stm);

        S = rec.NbOfSamples;
        F = F + length(1:fs:rec.NbOfFrames);
    end

    data = int16(zeros(S,F));
    stamps = zeros(F,1);
    pos = zeros(S,1);

    cnt = 1;

    for j = 1:M % Over all *.stm files    
        pth_stm = [pth_data filesep dir_dir(i).name filesep dir_stm(j).name];
        rec = stmopen(pth_stm);

        disp([num2str(cnt) ' - start of ' pth_stm]);

        for k = 1:fs:rec.NbOfFrames
            [data(:,cnt), stamps(cnt)] = stmread(rec, k);
            cnt = cnt + 1;
        end
    end

    data = data.';
    stamps = (stamps - min(stamps))/1e6;
    
    % Normalise the energy
    data = double(data)./repmat(sqrt(sum(abs(data.')).^2).',1,size(data,2));

    % Visualise the whole part
    figure;
    imagesc(pos,stamps,data);
    colormap jet;
    colorbar;
    xlabel('position [samples] \rightarrow');
    ylabel('\leftarrow time [s]');
    title(dir_dir(i).name);
    
    spec = 2*abs(fft(data.',NFFT)/size(data,2));
    spec = mean(spec.').';
    spec = movmean(spec,10);

    full_spec = [full_spec spec];
end

figure;
plot(full_spec(1:end/2,:));
grid on;
axis tight;
xlabel('k \rightarrow');
ylabel('S [k] \rightarrow');
legend(leg);