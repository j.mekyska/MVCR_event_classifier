%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
pth_data = {['..' filesep 'data' filesep 'labelled' filesep 'carAlong'] 'carAlong';...
    ['..' filesep 'data' filesep 'labelled' filesep 'groupRunningAcross'] 'groupRunningAcross';...
    ['..' filesep 'data' filesep 'labelled' filesep 'groupRunningAlong'] 'groupRunningAlong';...
    ['..' filesep 'data' filesep 'labelled' filesep 'groupWalkAcross'] 'groupWalkAcross';...
    ['..' filesep 'data' filesep 'labelled' filesep 'groupWalkAlong'] 'groupWalkAlong';...
    ['..' filesep 'data' filesep 'labelled' filesep 'hammer'] 'hammer';...
    ['..' filesep 'data' filesep 'labelled' filesep 'individualRunningAcross'] 'individualRunningAcross';...
    ['..' filesep 'data' filesep 'labelled' filesep 'individualRunningAlong'] 'individualRunningAlong';...
    ['..' filesep 'data' filesep 'labelled' filesep 'individualWalkAcross'] 'individualWalkAcross';...
    ['..' filesep 'data' filesep 'labelled' filesep 'individualWalkAlong'] 'individualWalkAlong';...
    ['..' filesep 'data' filesep 'labelled' filesep 'neutralArmy'] 'neutralArmy'};

pth_out = ['..' filesep 'data' filesep 'train'];

number_of_neighbors = 5;

h = [ ...
     1,      2,      4,      7,     10, ...
    13,     17,     21,     27,     32, ...
    38,     45,     52,     60,     68, ...
    77,     86,     96,    106,    117, ...
   128,    140,    153,    166,    179, ...
   193,    208,    223,    239,    255, ...
   272,    289,    307,    325,    344, ...
   363,    383,    404,    425,    446, ...
   468,    491,    514,    537,    562, ...
   586,    611,    637,    663,    690, ...
   718,    745,    774,    803,    832, ...
   862,    832,    803,    774,    745, ...
   718,    690,    663,    637,    611, ...
   586,    562,    537,    514,    491, ...
   468,    446,    425,    404,    383, ...
   363,    344,    325,    307,    289, ...
   272,    255,    239,    223,    208, ...
   193,    179,    166,    153,    140, ...
   128,    117,    106,     96,     86, ...
    77,     68,     60,     52,     45, ...
    38,     32,     27,     21,     17, ...
    13,     10,      7,      4,      2, ...
	1] / 32768;

%% Build the database
cnt_dat = 1;
cnt_mat = 1;

for i = 1:size(pth_data,1) % Over all data folders
    label = pth_data{i,2};    
    dir_dat = dir([pth_data{i,1} filesep '*.dat']);
    
    for j = 1:size(dir_dat,1) % Over all *.dat files
        % Parse the *.dat name
        prs = sscanf(dir_dat(j).name, 'L%dP%dT%dW%dR%dF%dH%dG%d.dat');
        
        time = prs(3);
        width = prs(5);
        fs = prs(6);
        threshold = prs(7);
        
        % Read the file and reshape it
        pth_dat = [pth_data{i,1} filesep dir_dat(j).name];
        disp(['Processing ' pth_dat]);
        
        FID = fopen(pth_dat, 'rb');
        data = fread(FID, 'int16');
        fclose(FID);

        data = data(1:floor(length(data)/width)*width);
        
        data = reshape(data,[],width);
        tosa = (1:size(data,1))./fs;
        wosa = 0:width-1;

        % Detect activation
        det = zeros(floor(10*size(data,1)/fs),size(data,2));
        
        for k = 1:size(det,1)
            seg = data((k-1)*round(fs/10) + (1:round(fs/10)),:);
            tmp = max(seg)-min(seg);
            tmp = conv(h,tmp);
            tmp = tmp((length(h)-1)/2 + (1:size(det,2)));
            det(k,:) = tmp;
        end
        
%         % Plot some figures
%         figure;
%         [~, ind] = max(max(det));
%         imagesc(wosa, tosa, data);
%         hold on;
%         plot([ind; ind],[tosa(1); tosa(end)],'r');
%         xlabel('width [segments]');
%         ylabel('time [s]')
%         title(['Raw data (' pth_dat ')']);
%         
%         figure;
%         imagesc(wosa, tosa, det);
%         xlabel('width [segments]');
%         ylabel('time [s]')
%         title(['Output of detector (' pth_dat ')']);
%         colorbar;

        % Select column with the biggest magnitude and its neighbors
        [~, ind] = max(max(det));
        ind = ind(1);
        
        brd_l = max([1 ind-number_of_neighbors]);
        brd_r = min([ind+number_of_neighbors size(data,2)]);
        
        data_trimmed = data(:,brd_l:brd_r);
        
        % Store the data
        for k = 1:size(data_trimmed,2) % Over all trimmed frames
            pth_mat = [label '_' num2str(cnt_mat) '.mat'];
            cnt_mat = cnt_mat + 1;
            
            frame.data = data_trimmed(:,k);
            frame.fs = fs;
            frame.filename = pth_mat;
            frame.original_filename = dir_dat(j).name;
            frame.ID = cnt_dat;
            frame.label = label;
            
            disp(['Storing ' pth_out filesep pth_mat]);
            save([pth_out filesep pth_mat], 'frame');
        end
        
        cnt_dat = cnt_dat + 1;
    end
end