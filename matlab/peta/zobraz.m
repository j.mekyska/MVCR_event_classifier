close all;
clear all;
clc;

filename = '*.dat';
dirname = 'H:\\';

%% prah pro rozkmit (max - min) signalu
%% vhodna volba je od 800 vys
prah = 1000;

h = [ ...
     1,      2,      4,      7,     10, ...
    13,     17,     21,     27,     32, ...
    38,     45,     52,     60,     68, ...
    77,     86,     96,    106,    117, ...
   128,    140,    153,    166,    179, ...
   193,    208,    223,    239,    255, ...
   272,    289,    307,    325,    344, ...
   363,    383,    404,    425,    446, ...
   468,    491,    514,    537,    562, ...
   586,    611,    637,    663,    690, ...
   718,    745,    774,    803,    832, ...
   862,    832,    803,    774,    745, ...
   718,    690,    663,    637,    611, ...
   586,    562,    537,    514,    491, ...
   468,    446,    425,    404,    383, ...
   363,    344,    325,    307,    289, ...
   272,    255,    239,    223,    208, ...
   193,    179,    166,    153,    140, ...
   128,    117,    106,     96,     86, ...
    77,     68,     60,     52,     45, ...
    38,     32,     27,     21,     17, ...
    13,     10,      7,      4,      2, ...
	1] / 32768;

% chci jeden soubor nebo vsechny v adresari?
if 0
  [filename, dirname, extidx] = uigetfile( {'*.dat', 'Soubory udalosti'}, 'Vyber udalost', dirname);
else
  dirname = uigetdir(dirname);
end

if(0 == dirname)
    return;
end

list = dir(fullfile(dirname, filename));

for k = 1:length(list)
    tmp = sscanf(list(k).name, 'L%dP%dT%dW%dF%d.dat');
    
    delka = tmp(1);
    pozice = tmp(2);
    cas = tmp(3);
    sirka = tmp(4);
    fvz = tmp(5);

%     if(5 ~= count)
%         warning( ['Nerozpoznany nazev ', dirname, list(k).name]);
%         continue;
%     end

    filename = fullfile(dirname, list(k).name);

    fid = fopen(filename, 'rb');
    if(0 >= fid)
        warning( ['Nelze otevrit soubor ', filename]);
        continue;
    end
 
    disp( ['Read ' num2str(k) '/' num2str( length(list)) ' ' list(k).name]);
    data = fread( fid, 'int16');
    fclose( fid);

    data = reshape( data,[], sirka);
    tosa = (0:size( data, 1)-1)/fvz;


    det = zeros( floor( 10*size( data, 1)/fvz), size( data,2));
    for m = 1:size(det, 1)
        seg = data( (m-1)*round( fvz/10) + (1:round( fvz/10)), :);
        tmp = max( seg)-min( seg);
        tmp = conv( h, tmp);
        tmp = tmp( (length( h)-1)/2 + (1:size( det, 2)));
        det(m,:) = tmp;
    end

    figure(1);
    %% zobrazeni surovych dat
    %imagesc( 0:sirka-1, tosa, data);
    %% zobrazení vystupu zjednoduseneho detektoru
    %imagesc( 0:sirka-1, tosa, det);
    %% zobrazeni vystupu detektoru s omezenim na velikost prahu
    %% sloupce, kde je vsude nejvyssi hodnota (barva) znamenaji, ze
    %% udalost byla detekovana celou dobu
    imagesc( 0:sirka-1, tosa, min( det, prah));
    title( list(k).name);

    figure(2);
    %% zobrazeni relativniho poctu 100ms segmentu, kdy udalost prekrocila
    %% nastaveny prah detektoru
    plot( [mean( det > prah); 0.8*ones( size( det))]');
    title(list(k).name);

    pause(1);
    if( prah < max( tmp))
        volba = questdlg( 'Ponechat soubor pro uceni?', 'Ponechat', 'Ano', 'Ne', 'Konec', 'Ano');
    else
        volba = questdlg( 'Ponechat soubor pro uceni?', 'Ponechat', 'Ano', 'Ne', 'Konec', 'Ne');
    end
    
    switch( volba)
        case 'Ne'
          pause(1);
        %      delete( fullfile( dirname, list(k).name));
        case 'Konec'
          return;
    end
end