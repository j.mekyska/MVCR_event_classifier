function [ frm, stamp] = stmread( rec, index)
  % velikost jednoho opakovani
  FrameSize = ( rec.SampleSize * rec.NbOfChannels * rec.NbOfSamples + rec.TailSize);
  if( 1 < nargin)
    % pri zadání indexu se musí v souboru posunout
    offset = rec.HeadSize + (index - 1) * FrameSize;
    
    if( 0 > fseek( rec.fid, offset))
      error( 'Out of file');
    end
  else
    index = ((ftell(rec.fid) - rec.HeadSize) / FrameSize + 1);
  end
  
  [frm, count] = fread( rec.fid, rec.NbOfChannels * rec.NbOfSamples, 'int16');
  if( count ~= rec.NbOfChannels * rec.NbOfSamples)
    error( 'Read error');
  end
  frm = 1.0/32768*reshape( frm, rec.NbOfChannels, [])';
  [tail, count] = fread( rec.fid, rec.TailSize / 8, 'int64');
  if( count ~= rec.TailSize / 8)
    error( 'Read Error');
  end
  
  stamp = rec.StartTime + (index - 1) * 1000000 / rec.FrameRate;  
end