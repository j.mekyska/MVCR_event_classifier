%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
pth_out = ['..' filesep '..' filesep 'data' filesep 'train'];

% Length of segments in seconds
len = 1;

% Sampling frequency
fs = 1e3;

% Number of samples
N = 200;

%% Generate data with uniform distribution
for i = 1:N
    rng(i);
    
    pth_mat = ['rand_' num2str(i) '.mat'];

    frame.data = rand(len*fs,1);
    frame.fs = fs;
    frame.filename = pth_mat;
    frame.original_filename = 'generated';
    frame.ID = i;
    frame.label = 'rand';

    disp(['Storing ' pth_out filesep pth_mat]);
    save([pth_out filesep pth_mat], 'frame');
end

%% Generate data with Gaussian distribution
for i = 1:N
    rng(i);
    
    pth_mat = ['randn_' num2str(i) '.mat'];

    frame.data = randn(len*fs,1);
    frame.fs = fs;
    frame.filename = pth_mat;
    frame.original_filename = 'generated';
    frame.ID = i;
    frame.label = 'randn';

    disp(['Storing ' pth_out filesep pth_mat]);
    save([pth_out filesep pth_mat], 'frame');
end