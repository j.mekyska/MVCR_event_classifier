%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(genpath(['..' filesep 'toolbox']));

pth_data = {['..' filesep '..' filesep 'data' filesep 'raw' filesep 'S1543494042000']};

%% Pair the data
for i = 1:size(pth_data,1) % Over all data folders
    % Get the list of *.xml files
    dir_xml = dir([pth_data{i,1} filesep '*.xml']);
    
    for j = 1:size(dir_xml,1) % Over all *.xml files
        desc = xml2struct([pth_data{i,1} filesep dir_xml(j).name]);
        
        table_out = {'lat' 'lon' 'event' 'id' 'time' 'time_posix'};
        
        for k = 1:size(desc.Records.rec, 2) % Over all labels            
            label = desc.Records.rec{1,k}.Attributes.event;
            
            tm = datetime(desc.Records.rec{1,k}.Attributes.time,...
                'InputFormat','yyyy-MM-dd''T''HH:mm:ss');
            tm = posixtime(tm);
            
            table_out = [table_out; {desc.Records.rec{1,k}.Attributes.lat ...
                desc.Records.rec{1,k}.Attributes.lon ...
                desc.Records.rec{1,k}.Attributes.event ...
                desc.Records.rec{1,k}.Attributes.id ...
                desc.Records.rec{1,k}.Attributes.time ...
                tm}];
        end
        
        % Store the *.csv file
        [filepath, name] = fileparts([pth_data{i,1} filesep dir_xml(j).name]);
        pth_out = fullfile(filepath, [name '.csv']);
        
        disp(['Storing ' pth_out]);
        cell2csv(pth_out, table_out);
    end
end