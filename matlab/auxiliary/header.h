typedef struct
{
	//! Total size, in Bytes, of the structure.
	uInt32	u32Size;   				//!<  Total size, in Bytes, of the structure.
	//! Total memory size of the each CompuScope board in the system in samples
	int64	i64MaxMemory;			//!<  Total memory size of each CompuScope board in the system in samples
	//! Vertical resolution, in bits, of the system
	uInt32	u32SampleBits;			//!<  Vertical resolution, in bits, of the system
	//! Sample resolution for the system. Used for voltage conversion.
	int32	i32SampleResolution;	//!<  Sample resolution for the system. Used for voltage conversion.
	//! Sample size, in Bytes.
	uInt32	u32SampleSize;			//!<  Sample size, in Bytes
	//! Sample offset of the system
	int32	i32SampleOffset;		//!<  This is ADC output that corresponds to the middle Voltage of the input range. Used for voltage conversion.
	//! Numeric constant that indicates CompuScope model
	uInt32	u32BoardType;			//!<  Numeric constant that indicates \link BOARD_TYPES CompuScope model \endlink
	//! Text string containing the CompuScope model name
	cschar	strBoardName[32];		//!<  Text string containing the CompuScope model name
	//! Options encoding features like gate, trigger enable, digital in, pulse out.
	uInt32	u32AddonOptions;		//!<  Options encoding features like gate, trigger enable, digital in, pulse out.
	//! Options encoding features like gate, trigger enable, digital in, pulse out.
	uInt32	u32BaseBoardOptions;	//!<  Options encoding features like gate, trigger enable, digital in, pulse out.
	//! Number of trigger engines available in the CompuScope system
	uInt32	u32TriggerMachineCount;	//!<  Number of trigger engines available in the CompuScope system
	//! Total number of channels in the CompuScope system
	uInt32 	u32ChannelCount;		//!<  Total number of channels in the CompuScope system
	//!Number of boards in the CompuScope system
	uInt32	u32BoardCount;  		//!<  Number of boards in the CompuScope system
	//!
} CSSYSTEMINFO, *PCSSYSTEMINFO;


typedef struct
{
	//! Total size, in Bytes, of the structure
	uInt32	u32Size;			//!< Total size, in Bytes, of the structure
	//! Sample rate value in Hz
	int64	i64SampleRate; 		//!< Sample rate value in Hz
	//! External clocking status.  A non-zero value means "active" and zero means "inactive"
	uInt32	u32ExtClk;			//!< External clocking status.  A non-zero value means "active" and zero means "inactive"
	//! Sample clock skip factor in external clock mode.
	uInt32	u32ExtClkSampleSkip;//!< Sample clock skip factor in external clock mode.  The sampling rate will be equal to
	                            //!< (external clocking frequency) / (u32ExtClkSampleSkip) * (1000). <BR>
	                            //!< For example, if the sample clock skip factor is 2000 then the sample rate will be one
	                            //!<  half of the external clocking frequency.
	//! Acquisition mode of the system
	uInt32	u32Mode;			//!< Acquisition mode of the system: \link ACQUISITION_MODES ACQUISITION_MODES\endlink.
	                            //!< Multiple selections may be ORed together.
	//! Vertical resolution of the CompuScope system
	uInt32  u32SampleBits;      //!< Actual vertical resolution, in bits, of the CompuScope system.
	//! Sample resolution for the CompuScope system
	int32	i32SampleRes;		//!< Actual sample resolution for the CompuScope system
	//! Sample size in Bytes for the CompuScope system
	uInt32	u32SampleSize;		//!< Actual sample size, in Bytes, for the CompuScope system
	//! Number of segments per acquisition.
	uInt32	u32SegmentCount;	//!< Number of segments per acquisition.
	//! Number of samples to capture after the trigger event
	int64	i64Depth;			//!< Number of samples to capture after the trigger event is logged and trigger delay counter has expired.
	//! Maximum possible number of points that may be stored for one segment acquisition.
	int64	i64SegmentSize;		//!< Maximum possible number of points that may be stored for one segment acquisition.
	                            //!< i64SegmentSize should be greater than or equal to i64Depth
	//! Amount of time to wait  after start of segment acquisition before forcing a trigger event.
	int64	i64TriggerTimeout;	//!< Amount of time to wait (in 100 nanoseconds units) after start of segment acquisition before
	                            //!< forcing a trigger event. CS_TIMEOUT_DISABLE means infinite timeout. Timeout counter is reset
	                            //!< for every segment in a Multiple Record acquisition.
	//! Enables the external signal used to enable or disable the trigger engines
	uInt32	u32TrigEnginesEn;	//!< Enables the external signal used to enable or disable the trigger engines
	//! Number of samples to skip after the trigger event before starting decrementing depth counter.
	int64	i64TriggerDelay;	//!< Number of samples to skip after the trigger event before starting to decrement depth counter.
	//! Number of samples to acquire before enabling the trigger circuitry.
	int64	i64TriggerHoldoff;	//!< Number of samples to acquire before enabling the trigger circuitry. The amount of pre-trigger
	                            //!< data is determined by i64TriggerHoldoff and has a maximum value of (i64RecordSize) - (i64Depth)
	//! Sample offset for the CompuScope system
	int32  i32SampleOffset;		//!< Actual sample offset for the CompuScope system
	//! Time-stamp mode.
	uInt32	u32TimeStampConfig;	//!< Time stamp mode: \link TIMESTAMPS_MODES TIMESTAMPS_MODES\endlink. 
	                            //!< Multiple selections may be ORed together.
	//! Number of segments per acquisition.
	int32	i32SegmentCountHigh;	//!< High patrt of 64-bit segment count. Number of segments per acquisition.
//!
} CSACQUISITIONCONFIG, *PCSACQUISITIONCONFIG;


typedef struct
{
	//!  Total size, in Bytes, of the structure
	uInt32	u32Size;			//!< Total size, in Bytes, of the structure
	//! Channel index.
	uInt32	u32ChannelIndex;	//!< Channel index. 1 corresponds to the first channel.
	//!< <BR>Regardless of the Acquisition mode, numbers are assigned to channels in a CompuScope system
	//!< as if they all are in use.<BR> For example, in an 8-channel system channels are numbered 1, 2, 3, 4, 5, 6, 7, 8.<BR>
    //!< All modes make use of channel 1. The rest of the channel numbers are evenly spaced throughout the CompuScope 
	//!< system by a fixed increment. To calculate this increment, users must divide the number of channels on one 
	//!< CompuScope board by the number of active channels in the current CompuScope mode, which is equal to the lower 
	//!< 12 bits of acquisition mode.
	
	//! Channel coupling and termination
    uInt32	u32Term;		    //!< Channel \link CHAN_DEFS coupling and termination\endlink. Used to set coupling (AC or DC),
	                            //!< the termination (single-ended or differential), and additional signal conditioning inputs such as Direct-to-ADC.
	//! Channel full scale input range, in mV peak-to-peak
    uInt32	u32InputRange;		//!< Channel full scale input range, in mV peak-to-peak.  May use \link CHAN_DEFS channel range\endlink constants
	//! Channel impedance, in Ohms
    uInt32	u32Impedance;		//!< Channel impedance, in Ohms (50 or 1000000)
	//! Channel bandwidth, kHz
	uInt32	u32Filter;			//!< Filter (default = 0 (No filter))
								//!< Index of the filter to be used. The filter parameters may be extracted with the CsGetSystemCaps using the CAPS_FILTERS parameter.
	//! Channel DC offset, in mV
	int32	i32DcOffset;		//!< Channel DC offset, in mV
	//! Channel on-board auto-calibration method
	int32	i32Calib;			//!< Channel on-board auto-calibration method. The default is 0. 
//!	
} CSCHANNELCONFIG, *PCSCHANNELCONFIG;


typedef struct
{
	//! Total size, in Bytes, of the structure.
	uInt32	u32Size;			//!< Total size, in Bytes, of the structure.
	//! Trigger engine index
	uInt32	u32TriggerIndex;	//!< Trigger engine index. 1 corresponds to the first trigger engine.
	//! Trigger condition
	uInt32	u32Condition;	   	//!< See \link TRIGGER_DEFS Trigger condition \endlink constants
	//! Trigger level as a percentage of the trigger source input range (±100%)
	int32	i32Level;			//!< Trigger level as a percentage of the trigger source input range (±100%)
	//! Trigger source
	int32	i32Source;			//!< See \link TRIGGER_DEFS Trigger source \endlink constants
	//! External trigger coupling
	uInt32	u32ExtCoupling;		//!< External trigger coupling: AC or DC
	//! External trigger range
	uInt32	u32ExtTriggerRange;	//!< External trigger full scale input range, in mV
	//! External trigger impedance
	uInt32	u32ExtImpedance;	//!< External trigger impedance, in Ohms
	//!
	int32	i32Value1;			//!< Reserved for future use
	//!
	int32	i32Value2;			//!< Reserved for future use
	//!
	uInt32	u32Filter;			//!< Reserved for future use
	//! Logical relation applied to the trigger engine outputs
	uInt32	u32Relation;     	//!< Logical relation applied to the trigger engine outputs (default OR)

} CSTRIGGERCONFIG, *PCSTRIGGERCONFIG;


typedef struct
{
	uInt32				u32Size;			// Size of this structure
	uInt16				u16CardIndex;		// Index of the card in the sytem
	uInt32				u32NbOfChannels;	// Number of Channel structures at the end of this structure
	uInt32				u32NbOfTriggers;	// Number of Triggers structures at the end of this structure
	uInt32				u32TailSize;		// Size of each segment tail in bytes

#ifdef _WINDOWS_
	unsigned __int64		u64StartSegment;	// Position of the first segment saved (If the file was paste from another, else 0)
	unsigned __int64		u64SegmentOffset;	// Offset in the first segment saved (If the file was paste from another, else 0)
	unsigned __int64		u64StartPoint;		// Start position of all the segments (If the file is the result of an extraction)
	unsigned __int64		u64EndPoint;		// End position of all the segments (If the file is the result of an extraction)
#else
	uint64				u64StartSegment;	// Position of the first segment saved (If the file was paste from another, else 0)
	uint64				u64SegmentOffset;	// Offset in the first segment saved (If the file was paste from another, else 0)
	uint64				u64StartPoint;		// Start position of all the segments (If the file is the result of an extraction)
	uint64				u64EndPoint;		// End position of all the segments (If the file is the result of an extraction)
#endif

	CSSYSTEMINFO			CSystemInfo;
	char					szDescription[256];
	CSACQUISITIONCONFIG		CsAcqConfig;

	// Následící pole pro vás už nejsou podstatné, je tam nastavený rozsah kanálů, úroveň spouštění, atd.
	//Dynamic section -- následuje pole jedné nebo více struktur konfigurace kanálu
	//CSCHANNELCONFIG channels[u32NbOfChannels];
	// a pole jedné nebo více struktur konfigurace spouštěče
	//CSTRIGGERCONFIG triggers[u32NbOfTriggers];
} STMHEADER, *PSTMHEADER;
