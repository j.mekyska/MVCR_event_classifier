function index = stmindex( rec)
  %% Vrati aktualni pozici ramce v souboru
  %% index -- aktualni poradi ramce v souboru
  %% rec   -- otevreny souboru
  
  FrameSize = ( rec.SampleSize * rec.NbOfChannels * rec.NbOfSamples + rec.TailSize);
  index = ((ftell(rec.fid) - rec.HeadSize) / FrameSize + 1);
end
