function rec = stmopen( file)
  fid = fopen( file, 'rb');
  if( -1 == fid)
    error('Cannot open file');
  end
  
  HeadSize = uint64( fread( fid, 1, 'uint32'));
  CardIndex = uint64(fread( fid, 1, 'uint16', 2));
  NbOfChannels = uint64( fread( fid, 1, 'uint32'));
  NbOfTriggers = uint64( fread( fid, 1, 'uint32'));
  TailSize = uint64( fread( fid, 1, 'uint32', 4));
  StartFrame = uint64(fread( fid, 1, 'uint64'));
  FrameOffset = uint64( fread( fid, 1, 'uint64'));
  StartPoint = uint64( fread( fid, 1, 'uint64'));
  EndPoint = uint64( fread( fid, 1, 'uint64', 88));
  %cssysteminfo = fread( fid, cssysteminfo);
  Description = char( fread( fid, 256, 'char'));
  Description = Description(:)';
  Size = uint64( fread( fid, 1, 'uint32', 4));
  SampleRate = uint64( fread( fid, 1, 'int64'));
  ExtClk = uint64( fread( fid, 1, 'uint32'));
  ExtClkSampleSkip = uint64( fread( fid, 1, 'uint32'));
  Mode = uint64( fread( fid, 1, 'uint32'));
  SampleBits = uint64( fread( fid, 1, 'uint32'));
  SampleRes = uint64( fread( fid, 1, 'int32'));
  SampleSize = uint64( fread( fid, 1, 'uint32'));
  SegmentCount = uint64( fread( fid, 1, 'uint32',4));
  Depth = int64( fread( fid, 1, 'int64'));
  NbOfSamples = uint64( fread( fid, 1, 'int64'));
  TriggerTimeout = int64( fread( fid, 1, 'int64'));
  TrigEnginesEn = uint64( fread( fid, 1, 'uint32', 4));
  TriggerDelay = int64( fread( fid, 1, 'int64'));
  TriggerHoldoff = int64( fread( fid, 1, 'int64'));
  SampleOffset = int64( fread( fid, 1, 'int32'));
  TimeStampConfig = uint64( fread( fid, 1, 'uint32'));
  SegmentCountHigh = int64( fread( fid, 1, 'int32', 4));
  
  % urceni poctu ramcu v souboru
  fseek(fid, 0, 'eof');
  filesize = ftell(fid);
  NbOfFrames = (filesize - HeadSize) / (NbOfChannels * SampleSize * NbOfSamples + TailSize);
  % urceni opakovaciho kmitoctu
  pat = 'f = (?<freq>\d+) Hz';
  FrameRate = regexp(Description, pat, 'names');
  if( isempty(FrameRate.freq))
    fclose( fid);
    error( 'Cannot read FrameRate');
  end
  FrameRate = str2num(FrameRate.freq);
  % urceni casu spusteni
  pat = 'T(?<time>\d+).stm';
  StartTime = regexp(file, pat, 'names');
  if( isempty(StartTime.time))
    StartTime.time = '0';
  end
  StartTime = uint64( str2num( StartTime.time));

  rec = struct( ...
    'fid', fid, ...
    'HeadSize', HeadSize, ...
    'SampleSize', SampleSize,...
    'NbOfChannels', NbOfChannels, ...
    'NbOfSamples', NbOfSamples,...
    'TailSize', TailSize, ...
    'NbOfFrames', NbOfFrames,...
    'SampleRate', SampleRate,...
    'FrameRate', FrameRate,...
    'StartTime', StartTime,...
    'Description', Description...
    
    );
  
  fseek( fid, HeadSize);
end
