%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
pth_data = ['..' filesep '..' filesep 'data' filesep 'extracted'];

%% Check the times
dir_folders = dir(pth_data);

for i = 3:size(dir_folders,1) % Over all data folders
    if(~dir_folders(i).isdir)
        continue;
    end
    
    dir_dat = dir([pth_data filesep dir_folders(i).name filesep '*.dat']);
    time = zeros(size(dir_dat,1),1);
    
    for j = 1:size(dir_dat,1) % Over all *.dat files
        % Parse the *.dat name
        prs = sscanf(dir_dat(j).name, 'L%dP%dT%dW%dF%dH%d.dat');
        time(j) = prs(3);
    end
    
    disp([dir_folders(i).name ' ' num2str(min(time)) ' ' num2str(max(time))]);
end