%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(['..' filesep 'toolbox']);

pth_data = ['..' filesep '..' filesep 'data' filesep 'old'];
pth_out = ['..' filesep '..' filesep 'data' filesep 'train'];

% Length of segments in seconds
len = 1;

%% Build the database
% Read the description file
if(exist([pth_data filesep 'description.xlsx'], 'file'))
    [~, ~, descr] = xlsread([pth_data filesep 'description.xlsx']);
else
    error(['Description file ' pth_data filesep ...
        'description.xlsx does not exist']);
end

% Copy the files to database
cnt_dat = 1;

for i = 2:size(descr,1) % Over all *.mat files
    pth_src = [pth_data filesep descr{i,1}];

    % Load and segment the data
    data = struct([]);
    load(pth_src);

    label = descr{i,2};

    for j = 1:size(data.data,2) % Over all data columns
        Y = segmentation(data.data(:,j), data.freq*len, 0, 0);

        % Store the segmented data
        for k = 1:size(Y,2) % Over all segments
            pth_mat = [label '_' num2str(cnt_dat) '.mat'];
            cnt_dat = cnt_dat + 1;
            
            frame.data = Y(:,k);
            frame.fs = data.freq;
            frame.filename = pth_mat;
            frame.original_filename = descr{i,1};
            frame.ID = cnt_dat;
            frame.label = label;
            
            disp(['Storing ' pth_out filesep pth_mat]);
            save([pth_out filesep pth_mat], 'frame');
        end
    end
end