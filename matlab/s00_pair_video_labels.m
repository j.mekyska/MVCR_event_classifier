%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(genpath('toolbox'));

pth_data = {['..' filesep 'data' filesep 'raw' filesep 'S1543494042000']};
pth_out = ['..' filesep 'data' filesep 'labelled'];

% Position
pos = 5450;

% Tolerance of position
tolerance_pos = 10;

% Tolerance in seconds
tolerance_time = 1;

%% Pair the data
for i = 1:size(pth_data,1) % Over all data folders
    % Get the list of *.xml files
    dir_xml = dir([pth_data{i,1} filesep '*.xml']);
    
    % Get the list of *.dat files
    dir_dat = dir([pth_data{i,1} filesep '*.dat']);
    
    % Parse the *.dat filenames
    % Length, position, time, width, fs, threshold
    data = cell(size(dir_dat,1), 6);
    
    for j = 1:size(dir_dat,1) % Over all *.dat files
        prs = sscanf(dir_dat(j).name, 'L%dP%dT%dW%dF%dH%d.dat');
        
        data{j,1} = prs(1);
        data{j,2} = prs(2);
        data{j,3} = prs(3);
        data{j,4} = prs(4);
        data{j,5} = prs(5);
        data{j,6} = prs(6);
    end
    
    % Filter data according to their position
    pos_vector = cell2mat(data(:,2));
    
    ind = find((pos_vector > (pos - tolerance_pos)) & (pos_vector < ...
        (pos + tolerance_pos)));
    
    if(isempty(ind))
        warning(['No detected events at position ' num2str(pos) ...
            ' (+- ' num2str(tolerance_pos) ') in ' pth_data{i,1}]);
        continue;
    end
    
    data = data(ind,:);
    
    % Convert *.dat times to one vector
    time_vector = cell2mat(data(:,3));

    % Pair the data
    cnt_missed = 1;
    cnt_all = 0;
    
    for j = 1:1%size(dir_xml,1) % Over all *.xml files
        desc = xml2struct([pth_data{i,1} filesep dir_xml(j).name]);
        
        for k = 1:size(desc.Records.rec, 2) % Over all labels
            cnt_all = cnt_all + 1;
            
            label = desc.Records.rec{1,k}.Attributes.event;
            
            tm = datetime(desc.Records.rec{1,k}.Attributes.time,...
                'InputFormat','yyyy-MM-dd''T''HH:mm:ss');
            tm = posixtime(tm);
            
            % Find possible time candidates among the *.dat files
            ind = find((time_vector > (tm - tolerance_time)) & ...
                (time_vector < (tm + tolerance_time)));
            
            if(isempty(ind))
                warning(['Cannot find event ' label ' at time ' ...
                    desc.Records.rec{1,k}.Attributes.time ...
                    ' (' num2str(cnt_missed) '/' num2str(cnt_all) ')']);
                cnt_missed = cnt_missed + 1;
                continue;
            end

        end
    end
end