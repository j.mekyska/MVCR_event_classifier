%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(genpath('toolbox'));

pth_data = ['..' filesep 'data' filesep 'train'];
pth_out = ['..' filesep 'data' filesep 'train_spectrogram'];

winlen = 0.020;
winover = 0.010;
NFFT = 1000;

% MUST BE COUPLED
dest_fs = 1000;
num_of_samples = 997;

% Define classes and max number of samples in each of them
classes = {'train' 0; ...
    'train2' 0; ...
    'cargoTrain' 0; ...
    'neutral' 30000; ...
    'crossing' 0; ...
    'kladivo' 0; ...
    'rezani' 0; ...
    'vrtani' 0; ...
    'rand' 0; ...
    'randn' 0};

% Random number generator
rng(1);

%% Randomly select maximum number of samples in each class
dir_mat = [];

for i = 1:size(classes,1) % Over all classes
    dir_tmp = dir([pth_data filesep classes{i,1} '_*.mat']);
    max_samples = classes{i,2};
    
    if((max_samples < Inf) && (max_samples < size(dir_tmp,1)))
        sel = randperm(size(dir_tmp,1));
        sel = sel(1:max_samples);

        dir_tmp = dir_tmp(sel);
    end
    
    disp(['Including ' num2str(size(dir_tmp,1)) ' filese with label ' ...
        classes{i,1}]);
    dir_mat = [dir_mat; dir_tmp];
end

%% Process the signals
res_list = {};

for i = 1:size(dir_mat,1) % Over all *.mat files
    disp(['Processing ' dir_mat(i).name ' (' num2str(i) '/' ...
        num2str(size(dir_mat,1)) ')']);
    
    % Load the *.mat file
    pth_mat = [pth_data filesep dir_mat(i).name];
    [filepath,name] = fileparts(pth_mat);
    pth_csv = fullfile(pth_out,[name '.csv']);
    
    if(exist(pth_csv,'file'))
        continue;
    end
    
    frame = struct([]);
    load(pth_mat);
    
    y = frame.data;
    fs = frame.fs;
    
    % Decimate     
    if(fs ~= dest_fs)
        G = gcd(fs, dest_fs);
        Q = fs/G;
        P = dest_fs/G;

        y = resample(y, P, Q);
        
        if(length(y) < num_of_samples)
            error(['The length of signal ' pth_mat ' after ' ...
                'resampling should be ' num2str(num_of_samples) ...
                ', but it is ' num2str(length(y))]);
        elseif(length(y) > num_of_samples)
            disp(['Trimming the length of ' pth_mat ' from ' ...
                num2str(length(y)) ' to ' num2str(num_of_samples)]);
            y = y(1:num_of_samples);
        end

        fs = dest_fs;
    end
    
    % Normalize
    y = y./max(abs(y));
    
    % Subtract mean
    y = y-mean(y);
    
    % Calculate the spectrogram
    s = my_spectrogram(y,hamming(fix(fs*winlen)),fix(winover*fs),NFFT);
    s = 20*log10(abs(s)+eps);
    
    % Show the spectrogram
%     [S, f, t] = spectrogram(y,hamming(fix(fs*winlen)),fix(winover*fs),NFFT,fs);
%     S = 20*log10(abs(S));
%     imagesc(t, f, S);
%     axis xy;
%     xlabel('{\it t} [s] \rightarrow');
%     ylabel('{\it f} [Hz] \rightarrow');
    
    % Feed the output table
    table = cell(1,6+numel(s));
    
    table{1,1} = frame.original_filename;
    table{1,2} = frame.filename;
    table{1,3} = frame.ID;
    table{1,4} = frame.label;
    table{1,5} = size(s,1);
    table{1,6} = size(s,2);
    table(1,7:end) = num2cell(reshape(s,1,[]));
    
    % Save the *.csv file
    disp(['Storing the table with results to ' pth_csv]);

    cell2csv(pth_csv, table, ',');
end