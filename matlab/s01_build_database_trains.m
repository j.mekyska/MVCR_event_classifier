%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
pth_data = {['..' filesep 'data' filesep 'labelled' filesep 'train'] 'train'; ...
    ['..' filesep 'data' filesep 'labelled' filesep 'neutral'] 'neutral'; ...
    ['..' filesep 'data' filesep 'labelled' filesep 'cargoTrain'] 'cargoTrain'};

pth_out = ['..' filesep 'data' filesep 'train'];

% Threshold for oscilation (min - max) of signal. It is recommended to set
% it above 800
threshold = 2000;

% Minimum number of 100 ms frames in one length segment that must contain
% activation so that it can be kept for further processing
pr = 8;

% Use script check_times to generate these times from folder extracted
scenarios = {'krizanov' 1539158813 1539161116; ...
    'kolin' 1539161325 1539162307; ...
    'kolin' 1539167154 1539170915; ...
    'kolin' 1539171054 1539172110; ...
    'kolin' 1539173041 1539173920};

train_station = {
    'krizanov' [0 300; 4100 4550; 8200 9000; 13400 13800; 17400 18200; ...
    31000 33000];
    'kolin' [0 2600; 5000 6000; 8500 10500; 17000 17200; 18400 18800; ...
    22400 22800; 29800 30800; 34400 36000; 38000 42000];};

h = [ ...
     1,      2,      4,      7,     10, ...
    13,     17,     21,     27,     32, ...
    38,     45,     52,     60,     68, ...
    77,     86,     96,    106,    117, ...
   128,    140,    153,    166,    179, ...
   193,    208,    223,    239,    255, ...
   272,    289,    307,    325,    344, ...
   363,    383,    404,    425,    446, ...
   468,    491,    514,    537,    562, ...
   586,    611,    637,    663,    690, ...
   718,    745,    774,    803,    832, ...
   862,    832,    803,    774,    745, ...
   718,    690,    663,    637,    611, ...
   586,    562,    537,    514,    491, ...
   468,    446,    425,    404,    383, ...
   363,    344,    325,    307,    289, ...
   272,    255,    239,    223,    208, ...
   193,    179,    166,    153,    140, ...
   128,    117,    106,     96,     86, ...
    77,     68,     60,     52,     45, ...
    38,     32,     27,     21,     17, ...
    13,     10,      7,      4,      2, ...
	1] / 32768;

%% Build the database
cnt_dat = 1;
cnt_mat = 1;

scenario_brd = cell2mat(scenarios(:,2:end));

for i = 1:size(pth_data,1) % Over all data folders
    label = pth_data{i,2};    
    dir_dat = dir([pth_data{i,1} filesep '*.dat']);
    
    for j = 1:size(dir_dat,1) % Over all *.dat files
        % Parse the *.dat name
        prs = sscanf(dir_dat(j).name, 'L%dP%dT%dW%dF%dH%d.dat');
        
        len = prs(1);
        position = prs(2);
        time = prs(3);
        width = prs(4);
        fs = prs(5);
        
        if(length(prs) > 5)
            threshold = prs(6);
        end
        
        % Check whether the sample comes from a train station
        sel = find((time >= scenario_brd(:,1)) & ...
            (time <= scenario_brd(:,2)));
        
        if(~isempty(sel))
            ind = find(strcmp(train_station(:,1), scenarios{sel,1}));
            stations = train_station{ind,2};
            
            in_station = any((position >= stations(:,1)) & ...
                (position <= stations(:,2)));
            
            if(in_station)
                continue;
            end
        end
        
        % Read the file and reshape it
        pth_dat = [pth_data{i,1} filesep dir_dat(j).name];
        disp(['Processing ' pth_dat]);
        
        FID = fopen(pth_dat, 'rb');
        data = fread(FID, 'int16');
        fclose(FID);

        data = reshape(data,[],width);
        tosa = (0:size(data,1)-1)/fs;
        wosa = 0:width-1;

        % Detect activation
        det = zeros(floor(10*size(data,1)/fs),size(data,2));
        
        for k = 1:size(det,1)
            seg = data((k-1)*round(fs/10) + (1:round(fs/10)),:);
            tmp = max(seg)-min(seg);
            tmp = conv(h,tmp);
            tmp = tmp((length(h)-1)/2 + (1:size(det,2)));
            det(k,:) = tmp;
        end
        
        % Select data only with the activation        
        sel = sum(det > threshold) >= pr;
        data_trimmed = data(:,sel);
        
        if(isempty(data_trimmed))
            continue;
        end
        
        % Plot some figures
%         figure;
%         imagesc(wosa, tosa, data);
%         xlabel('width [segments]');
%         ylabel('time [s]')
%         title(['Raw data (' pth_dat ')']);
%         
%         figure;
%         imagesc(wosa, tosa, det);
%         xlabel('width [segments]');
%         ylabel('time [s]')
%         title(['Output of detector (' pth_dat ')']);
%         colorbar;
%         
%         figure;
%         imagesc(wosa, tosa, min(det, threshold));
%         xlabel('width [segments]');
%         ylabel('time [s]')
%         title(['Output of detector lower than threshold = ' ...
%             num2str(threshold)]);
%         colorbar;
%         
%         figure;
%         imagesc(wosa(sel), tosa, data_trimmed);
%         xlabel('width [segments]');
%         ylabel('time [s]')
%         title(['Selected data (' pth_dat ')']);

        % Select column with the biggest range
        [~, ind] = max(range(data_trimmed));
        data_trimmed = data_trimmed(:,ind(1));
        
        % Store the data
        for k = 1:size(data_trimmed,2) % Over all trimmed frames
            pth_mat = [label '_' num2str(cnt_mat) '.mat'];
            cnt_mat = cnt_mat + 1;
            
            frame.data = data_trimmed(:,k);
            frame.fs = fs;
            frame.filename = pth_mat;
            frame.original_filename = dir_dat(j).name;
            frame.ID = cnt_dat;
            frame.label = label;
            
            disp(['Storing ' pth_out filesep pth_mat]);
            save([pth_out filesep pth_mat], 'frame');
        end
        
        cnt_dat = cnt_dat + 1;
    end
end