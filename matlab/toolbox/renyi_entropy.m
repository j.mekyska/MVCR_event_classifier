function H = renyi_entropy(y, q, prec)

% H = renyi_entropy(y, q, prec)
% 
% This function calculates the Renyi entropy of input vector.
% 
% y     - input column vector
% q     - oder (default: 2)
% prec  - precision of calculation; if prec = -1 then the precision will be
%         maximal (default: depending on the length of vector)
% H     - output entropy

%% Paths and variables
if((nargin < 2) || isempty(q))
    q = 2;
end
if((nargin < 3) || isempty(prec))
    if(length(y) < 64)
        prec = 32;
    elseif(length(y) < 128)
        prec = 64;
    else
        prec = 256;
    end
end

%% Calculate the probability
if(prec > 0)
    p = hist(y, prec);
    p = p./sum(p);
    p = p(p > 0);
else
    [tmp1, tmp2, n] = unique(y);
    
    p = hist(n,max(n));
    p = p./length(y);
end

%% Calculate the entropy
if(q == 1)
    H = -sum(p.*log2(p));
else
    H = (1/(1-q))*log2(sum(p.^q));
end