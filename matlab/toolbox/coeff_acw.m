function coef = coeff_acw(lpc)

% coef = coeff_acw(lpc)
% 
% This function calculates the Adaptive Component Weighted (ACW) cepstral
% coefficients.
% 
% lpc   - matrix with LPC coefficients, segments are related to the columns
% coef  - matrix with ACW coefficients, segments are related to the columns

%% Get the order of LPC
p = size(lpc,1)-1;

%% Calculate the channel LPC
chn = repmat(((p-(0:p))./p).',1,size(lpc,2)).*lpc;

%% Calculate the ACW
coef = zeros(p+1,size(lpc,2));
coef(1,:) = log(p);

lpcc_s = lpc2cep(lpc);
lpcc_c = lpc2cep(chn);

coef(2:end,:) = lpcc_s - lpcc_c;