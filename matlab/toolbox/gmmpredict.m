function [cls, prob] = gmmpredict(obj, data)

% [cls, prob] = gmmpredict(obj, data)
% 
% This function classify the input data using the trained GMM models.
% 
% obj   - structure with the trained GMM models
% data  - source data where each column corresponds to feature and each
%         row corresponds to person
% cls   - output column vector with numeric labels
% prob  - output classification scores (first line corresponds to labels,
%         the rest corresponds to people)

%% Test the models
prob = zeros(size(data,1)+1,size(obj,2));

for i = 1:size(obj,2) % Over all labels
    mix = obj(i).model;
    
    prob(2:end,i) = gmmprob(mix, data);
    prob(1,i) = obj(i).label;
end

%% Find the highest probabilities
[~, ind] = max(prob(2:end,:).');
cls = prob(1,ind).';