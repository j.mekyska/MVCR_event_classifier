function fm = hz2mel(f)

% fm = hz2mel(f)
% 
% This function converts hertz scale to mel scale.
% 
% f     - hertz scale
% fm    - output scale in mel frequency

%% Convert the scales
fm = 2595.*log10(1 + f./700);