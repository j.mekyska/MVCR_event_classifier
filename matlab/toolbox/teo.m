function TEO = teo(y)

% TEO = teo(y)
% 
% This function calculates the Teager-Kaiser operator.
% 
% y     - input column signal
% TEO   - output Teager-Kaiser operator

%% Calculate the Teager-Kaiser operator
TEO = y(2:end-1).^2-y(1:end-2).*y(3:end);