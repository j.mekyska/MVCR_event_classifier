function f = mel2hz(fm)

% f = mel2hz(fm)
% 
% This function convert the mel scale to hertz scale.
% 
% fm    - mel scale
% f     - output hertz scale

%% Convert the scales
f = 700.*(10.^(fm./2595)-1);