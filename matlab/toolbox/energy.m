function E = energy(y, winlen, winover, padd)

% E = energy(y, winlen, winover, padd)
% 
% This function calculates the short-time energy of input signal.
% 
% y         - input column vector
% winlen    - length of window in samples or window samples
% winover   - window overlapp in samples
% padd      - if set to 1, the last segment will be padded by zeros if
%             necessary (default: 0)
% E         - short-time energy

%% Paths and variables
if((nargin < 4) || isempty(padd))
    padd = 0;
end

%% Segment the signal
Y = segmentation(y, winlen, winover, padd);

%% Calculate the energy
E = sum(abs(Y).^2);