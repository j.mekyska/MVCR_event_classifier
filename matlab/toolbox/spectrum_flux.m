function SF = spectrum_flux(y, fs, wind, winover, padd, per)

% SF = spectrum_flux(y, fs, wind, winover, padd, per)
% 
% This function calculates the spectrum flux.
% 
% y         - input column vector
% fs        - sampling frequency
% wind      - length of window in samples or window samples
% winover   - window overlapp in samples
% padd      - if set to 1, the last segment will be padded by zeros if
%             necessary (default: 0)
% per       - period (in seconds) over which will be SF calculated
%             (default: 0.25)
% SF        - output row vector with spectrum flux
% 
% Implemented according to
% Hao Jiang, Lie Lu, and HongJiang Zhang. A Robust Audio Classification and
% Segmentation Method. 2001.

%% Paths and variables
if((nargin < 5) || (isempty(padd)))
    padd = 0;
end
if((nargin < 6) || (isempty(per)))
    per = 0.25;
end

delta = 1e-9;

%% Check the signal length
if((length(y)/fs) < 3*per)
%     warning(['Spectrum Flux: input signal is shorter ' ...
%         'than ' num2str(3*per) 's. SF will be set to an empty value.']);
    SF = [];
    return;
end

%% Segment the signal
Y = segmentation(y, wind, winover, padd);

%% Calculate the magnitude spectrum
S = abs(fft(Y));
S = S(1:fix(end/2),:);

%% Reshape the segmented signal to seconds
% Number of frames in one period
n_frames = fix(((fs/winover)-1)*per);

% Number of periods
n_per = fix(size(Y,2)/n_frames);

% Trim the segments
S = S(:,1:n_frames*n_per);

% Get number of rows
n_row = size(S,1);

tmp = repmat(1:n_frames,n_per,1)+repmat(((0:n_per-1)*n_frames).',1,n_frames);
S = reshape(S(:,tmp), [], n_frames);

%% Calculate the spectrum flux
SF = (log(S(:,2:end)+delta)-log(S(:,1:end-1)+delta)).^2;
SF = mean(SF.');
SF = reshape(SF, [], n_row).';
SF = mean(SF);