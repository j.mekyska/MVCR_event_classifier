function S = my_spectrogram(y, wind, winover, NFFT)

% This function returns spectrogram of the input signal.
% 
% y         - input column vector
% wind      - length of window in samples or window samples
% winover   - window overlapp in samples
% NFFT      - number of FFT points
% 
% S         - complex spectrogram

%% Segment the signal
Y = segmentation(y, wind, winover, 0);

%% Calculate the spectrogram
S = fft(Y, NFFT);
S = S(1:1+fix(size(S,1)/2),:);