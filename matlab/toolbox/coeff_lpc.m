function coef = coeff_lpc(y, fs, wind, winover, padd, p)

% coef = coeff_lpc(y, fs, wind, winover, padd, p)
% 
% This function returns the Linear Predictive Coefficients (LPC).
% 
% y         - input column vector
% fs        - sampling frequency
% wind      - length of window in samples or window samples
% winover   - window overlapp in samples
% padd      - if set to 1, the last segment will be padded by zeros if
%             necessary (default: 0)
% p         - order of linear prediction (default: fix(fs/1e3)+2)
% coef      - matrix with LPC coefficients, segments are related to the
%             columns

%% Paths and variables
if((nargin < 5) || isempty(padd))
    padd = 0;
end
if((nargin < 6) || isempty(p))
    p = fix(fs/1e3)+2;
end

%% Segment the input signal
Y = segmentation(y, wind, winover, padd);

%% Get the LPC coefficients
coef = my_lpc_mtx(Y,p).';