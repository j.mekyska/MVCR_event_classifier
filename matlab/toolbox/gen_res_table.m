function gen_res_table(pth_results, res_list, pth_out, clean)

% gen_res_table(pth_results, res_list, pth_out, clean)
% 
% This function generates from results stored in *.mat files the final
% table.
% 
% pth_results   - path to the folder with results
% res_list      - list of *.mat files to be processed
% pth_out       - path to *.mat file containing the final table
% clean         - discard NaN, Inf, complex, same and empty values
%                 (default: 1)

%% Paths and variables
if((nargin < 4) || isempty(clean))
    clean = 1;
end

%% Get the list of features
pth_mat = [pth_results filesep res_list{1,1}];
frame = struct([]);
load(pth_mat);

list_all = frame.features.desc;

%% Create and feed the table
disp('Feeding the table');
table = cell(size(res_list,1)+1,length(list_all)+4);
table(1,:) = [{'original filename' 'filename' 'ID' 'label'} list_all];

for i = 1:size(res_list,1) % Over all results
    tic;
    
    % Load the results
    pth_mat = [pth_results filesep res_list{i,1}];
    frame = struct([]);
    load(pth_mat);
    
    % Load the features
    table{i+1,1} = frame.original_filename;
    table{i+1,2} = frame.filename;
    table{i+1,3} = frame.ID;
    table{i+1,4} = frame.label;
    table(i+1,5:end) = num2cell(frame.features.data(1:length(list_all)).');
    
    tmr = toc;
    disp(['Feature ' num2str(i) '/' num2str(size(res_list,1)) ...
        ' added in ' num2str(tmr) ' s']);
end

%% Clean the table
if(clean)
    % Skip columns with empty values
    sel = [0 0 0 0 sum(cellfun(@(x) isempty(x), table(2:end,5:end)))] == 0;
    table = table(:,sel);

    % Skip columns with NaN values
    sel = [0 0 0 0 sum(cellfun(@(x) isnan(x), table(2:end,5:end)))] == 0;
    table = table(:,sel);

    % Skip columns with Inf values
    sel = [0 0 0 0 sum(cellfun(@(x) isinf(x), table(2:end,5:end)))] == 0;
    table = table(:,sel);

    % Skip columns with complex values
    sel = [0 0 0 0 sum(cellfun(@(x) ~isreal(x), table(2:end,5:end)))] == 0;
    table = table(:,sel);

    % Skip columns with same values
    sel = [1 1 1 1 range(cell2mat(table(2:end,5:end)))] ~= 0;
    table = table(:,sel);
end

%% Save the table to *.mat and *.csv files
% disp(['Storing the table with results to ' pth_out]);
% save(pth_out, 'table', '-v7.3');

[filepath,name] = fileparts(pth_out);
pth_out = fullfile(filepath,[name '.csv']);
disp(['Storing the table with results to ' pth_out]);

if(exist(pth_out,'file'))
    delete(pth_out);
end

cell2csv(pth_out, table, ',');