function [pres, row] = check_feature(features, feat)

% [pres, row] = check_feature(features, feat)
% 
% This function checks whether the feature should be calculated or not.
% 
% features  - cell table with the fetaures
% feat      - feature ID
% pres      - 1 if the feature should be calculated, otherwise 0
% row       - feature row index

%% Check the presence of feature
row = -1;

for i = 1:size(features,1) % Over all features
    if(strcmp(features{i,1}, feat))
        row = i;
        break;
    end
end

if(row == -1)
    error(['Feature ' '' feat '' ' is not present in the feature list.']);
elseif(length(row) > 1)
    error(['Multiple occurences of feature ' '' feat '' ...
        ' in the feature list.']);
else
    pres = features{row,3};
end