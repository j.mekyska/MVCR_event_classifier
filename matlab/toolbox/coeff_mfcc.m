function coef = coeff_mfcc(y, fs, wind, winover, padd, NFFT, p, eq)

% coef = coeff_mfcc(y, fs, wind, winover, padd, NFFT, p, eq)
% 
% This function returns the Mel Frequency Cepstral Coefficients (MFCC).
% 
% y         - input column vector
% fs        - sampling frequency
% wind      - length of window in samples or window samples
% winover   - window overlapp in samples
% padd      - if set to 1, the last segment will be padded by zeros if
%             necessary (default: 0)
% NFFT      - number of FFT points (default: 2048)
% p         - number of filters in bank (default: depending on fs)
% eq        - if set to 1 the eq. loud. curves will be applied (default: 0)
% coef      - matrix with MFCC coefficients, segments are related to the
%             columns

%% Paths and variables
if((nargin < 5) || (isempty(padd)))
    padd = 0;
end
if((nargin < 6) || (isempty(NFFT)))
    NFFT = 2048;
end
if((nargin < 7) || (isempty(p)))
    if(fs <= 8e3)
        p = 15;
    elseif(fs <= 11e3)
        p = 17;
    elseif(fs <= 16e3)
        p = 20;
    elseif(fs <= 22e3)
        p = 22;
    else
        p = 27;
    end
end
if((nargin < 8) || (isempty(eq)))
    eq = 0;
end

%% Segment the input signal
Y = segmentation(y, wind, winover, padd);

%% Calculate the power spectrum
P = abs(fft(Y,NFFT)).^2;
P = P(1:ceil(end/2),:);

%% Get the bank of filters
M = melbank(fs, size(P,1), p, eq);

%% Apply the bank and summ the samples
c = M*P;

%% Apply the natural logarithm
c = log(c);

%% Calculate the discrete cosine transform
coef = dct(c);

%% Calculate the logarithm of energy
coef(1,:) = log(abs(sum(Y)).^2);