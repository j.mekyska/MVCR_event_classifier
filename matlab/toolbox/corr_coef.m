function c = corr_coef(x, y)

% c = corr_coef(x, y)
% 
% This function calculates the first correlation coefficient of vectors x
% and y.
% 
% x - first input vector
% y - second input vector
% c - first correlation coefficient

%% Paths and variables
if((nargin < 2) || isempty(y))
    y = x(2:end);
    x = x(1:end-1);
end

%% Check the size
if(size(x,1) ~= size(y,1))
    error('Vectors ''x'' and ''y'' must have the same number of rows.');
end
if(size(x,2) ~= size(y,2))
    error('Vectors ''x'' and ''y'' must have the same number of columns.');
end

%% Calculate the first correlation coefficient
c = mean((x-mean(x)).*(y-mean(y)))/(std(x)*std(y));