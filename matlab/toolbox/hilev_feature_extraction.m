function [out, desc] = hilev_feature_extraction(data)

% out = hilev_feature_extraction(data)
% 
% This function calculates the high-level features from an input array.
% 
% data  - input column vector or a matrix. If the input is matrix, then the
%         features are calculated for each column.
% 
% out   - output column vector or matrix. Each row corresponds to one
%         feature (see variable desc)
% desc  - description of extracted features

%% Paths and variables
desc = {'max.'; ...
    'min.'; ...
    'position of max.'; ...
    'position of min.'; ...
    'relative position of max.'; ...
    'relative position of min.'; ...
    'range'; ...
    'relative range'; ...
    'relative variation range'; ...
    'interquartile range'; ...
    'relative interquartile range'; ...
    'interdecile range'; ...
    'relative interdecile range'; ...
    'interpercentile range'; ...
    'relative interpercentile range'; ...
    'studentized range'; ...
    'mean'; ...
    'harmonic mean'; ...
    'mean excluding 10% outliers'; ...
    'mean excluding 20% outliers'; ...
    'mean excluding 30% outliers'; ...
    'mean excluding 40% outliers'; ...
    'mean excluding 50% outliers'; ...
    'median'; ...
    'mode'; ...
    'var'; ...
    'std'; ...
    'mean absolute deviation'; ...
    'median absolute deviation'; ...
    'relative std'; ...
    'index of dispersion'; ...
    '3rd moment'; ...
    '4th moment'; ...
    '5th moment'; ...
    '6th moment'; ...
    'kurtosis'; ...
    'skewness'; ...
    'Pearson''s 1st skewness coeff.'; ...
    'Pearson''s 2nd skewness coeff.'; ...
    '1st percentile'; ...
    '5th percentile'; ...
    '10th percentile'; ...
    '20th percentile'; ...
    '1st quartile'; ...
    '30th percentile'; ...
    '40th percentile'; ...
    '60th percentile'; ...
    '70th percentile'; ...
    '3rd quartile'; ...
    '80th percentile'; ...
    '90th percentile'; ...
    '95th percentile'; ...
    '99th percentile'; ...
    'slope'; ...
    'offset of linear regression'; ...
    'error of linear regression'; ...
    'Shannon entropy'; ...
    '2nd order Renyi entropy'; ...
    'modulation'; ...
    'first correlation coefficient'};

%% Calculate the high-level features      
if(isrow(data))
    error('Input must be a column vector or matrix.');
end

% Initialize the output array
out = zeros(size(desc,1), size(data, 2));


for j = 1:size(data, 2) % Over all columns
    cnt = 1;
    
    % Prepare vector
    vec = data(:,j);

    % Prepare frequently used values
    data_len = length(vec);
    data_max = max(vec);
    data_min = min(vec);
    data_range = data_max-data_min;
    data_var = var(vec);
    data_std = std(vec);
    data_mean = mean(vec);
    data_mode = mode(vec);
    data_median = median(vec);

    % Calculate MAX
    out(cnt,j) = data_max;
    cnt = cnt + 1;

    % Calculate MIN
    out(cnt,j) = data_min;
    cnt = cnt + 1;

    % Calculate POSITION OF MAX
    pos_max = find(vec == data_max);
    pos_max = pos_max(1);
    out(cnt,j) = pos_max;
    cnt = cnt + 1;

    % Calculate POSITION OF MIN
    pos_min = find(vec == data_min);
    pos_min = pos_min(1);
    out(cnt,j) = pos_min;
    cnt = cnt + 1;

    % Calculate RELATIVE POSITION OF MAX
    pos_max = find(vec == data_max);
    pos_max = pos_max(1);
    out(cnt,j) = pos_max/data_len;
    cnt = cnt + 1;

    % Calculate RELATIVE POSITION OF MIN
    pos_min = find(vec == data_min);
    pos_min = pos_min(1);
    out(cnt,j) = pos_min/data_len;
    cnt = cnt + 1;

    % Calculate RANGE
    out(cnt,j) = data_range;
    cnt = cnt + 1;

    % Calculate RELATIVE RANGE
    out(cnt,j) = data_range/data_max;
    cnt = cnt + 1;

    % Calculate RELATIVE VARIATION RANGE
    out(cnt,j) = data_range/data_mean;
    cnt = cnt + 1;

    % Calculate INTERQUARTILE RANGE
    out(cnt,j) = iqr(vec);
    cnt = cnt + 1;

    % Calculate RELATIVE INTERQUARTILE RANGE
    out(cnt,j) = iqr(vec)/data_max;
    cnt = cnt + 1;

    % Calculate INTERDECILE RANGE
    out(cnt,j) = quantile(vec,0.9) - quantile(vec,0.1);
    cnt = cnt + 1;

    % Calculate RELATIVE INTERDECILE RANGE
    out(cnt,j) = (quantile(vec,0.9) - quantile(vec,0.1))/data_max;
    cnt = cnt + 1;

    % Calculate INTERPERCENTILE RANGE
    out(cnt,j) = quantile(vec,0.99) - quantile(vec,0.01);
    cnt = cnt + 1;

    % Calculate RELATIVE INTERPERCENTILE RANGE
    out(cnt,j) = (quantile(vec,0.99) - quantile(vec,0.01))/data_max;
    cnt = cnt + 1;

    % Calculate STUDENTIZED RANGE
    out(cnt,j) = data_range/data_var;
    cnt = cnt + 1;

    % Calculate MEAN
    out(cnt,j) = data_mean;
    cnt = cnt + 1;

    % Calculate HARMONIC MEAN
    out(cnt,j) = harmmean(vec);
    cnt = cnt + 1;

    % Calculate MEAN EXCLUDING OUTLIERS (10)
    out(cnt,j) = my_trimmean(vec, 10);
    cnt = cnt + 1;

    % Calculate MEAN EXCLUDING OUTLIERS (20)
    out(cnt,j) = my_trimmean(vec, 20);
    cnt = cnt + 1;

    % Calculate MEAN EXCLUDING OUTLIERS (30)
    out(cnt,j) = my_trimmean(vec, 30);
    cnt = cnt + 1;

    % Calculate MEAN EXCLUDING OUTLIERS (40)
    out(cnt,j) = my_trimmean(vec, 40);
    cnt = cnt + 1;

    % Calculate MEAN EXCLUDING OUTLIERS (50)
    out(cnt,j) = my_trimmean(vec, 50);
    cnt = cnt + 1;

    % Calculate MEDIAN
    out(cnt,j) = data_median;
    cnt = cnt + 1;

    % Calculate MODE
    out(cnt,j) = data_mode;
    cnt = cnt + 1;

    % Calculate VAR
    out(cnt,j) = data_var;
    cnt = cnt + 1;

    % Calculate STD
    out(cnt,j) = data_std;
    cnt = cnt + 1;

    % Calculate MEAN ABSOLUTE DEVIATION
    out(cnt,j) = mad(vec);
    cnt = cnt + 1;

    % Calculate MEDIAN ABSOLUTE DEVIATION
    out(cnt,j) = mad(vec,1);
    cnt = cnt + 1;

    % Calculate RELATIVE STANDARD DEVIATION
    out(cnt,j) = data_std/data_mean;
    cnt = cnt + 1;

    % Calculate INDEX OF DISPERSION
    out(cnt,j) = data_var/data_mean;
    cnt = cnt + 1;

    % Calculate 3rd MOMENT
    out(cnt,j) = moment(vec,3);
    cnt = cnt + 1;

    % Calculate 4th MOMENT
    out(cnt,j) = moment(vec,4);
    cnt = cnt + 1;

    % Calculate 5th MOMENT
    out(cnt,j) = moment(vec,5);
    cnt = cnt + 1;

    % Calculate 6th MOMENT
    out(cnt,j) = moment(vec,6);
    cnt = cnt + 1;

    % Calculate KURTOSIS
    out(cnt,j) = kurtosis(vec);
    cnt = cnt + 1;

    % Calculate SKEWNESS
    out(cnt,j) = skewness(vec);
    cnt = cnt + 1;

    % Calculate PEARSON'S 1st SKEWNESS COEFFICIENT
    out(cnt,j) = (3*(data_mean-data_mode))/data_std;
    cnt = cnt + 1;

    % Calculate PEARSON'S 2nd SKEWNESS COEFFICIENT
    out(cnt,j) = (3*(data_mean-data_median))/data_std;
    cnt = cnt + 1;

    % Calculate 1st PERCENTILE
    out(cnt,j) = prctile(vec,1);
    cnt = cnt + 1;

    % Calculate 5th PERCENTILE
    out(cnt,j) = prctile(vec,5);
    cnt = cnt + 1;

    % Calculate 10th PERCENTILE
    out(cnt,j) = prctile(vec,10);
    cnt = cnt + 1;

    % Calculate 20th PERCENTILE
    out(cnt,j) = prctile(vec,20);
    cnt = cnt + 1;

    % Calculate 1st QUARTILE
    out(cnt,j) = prctile(vec,25);
    cnt = cnt + 1;

    % Calculate 30th PERCENTILE
    out(cnt,j) = prctile(vec,30);
    cnt = cnt + 1;

    % Calculate 40th PERCENTILE
    out(cnt,j) = prctile(vec,40);
    cnt = cnt + 1;

    % Calculate 60th PERCENTILE
    out(cnt,j) = prctile(vec,60);
    cnt = cnt + 1;

    % Calculate 70th PERCENTILE
    out(cnt,j) = prctile(vec,70);
    cnt = cnt + 1;

    % Calculate 3rd QUARTILE
    out(cnt,j) = prctile(vec,75);
    cnt = cnt + 1;

    % Calculate 80th PERCENTILE
    out(cnt,j) = prctile(vec,80);
    cnt = cnt + 1;

    % Calculate 90th PERCENTILE
    out(cnt,j) = prctile(vec,90);
    cnt = cnt + 1;

    % Calculate 95th PERCENTILE
    out(cnt,j) = prctile(vec,95);
    cnt = cnt + 1;

    % Calculate 99th PERCENTILE
    out(cnt,j) = prctile(vec,99);
    cnt = cnt + 1;

    % Calculate SLOPE OF LINEAR REGRESSION
    [beta, alpha, err] = linear_regress_stat(vec);
    out(cnt,j) = beta;
    cnt = cnt + 1;

    % Calculate OFFSET OF LINEAR REGRESSION
    out(cnt,j) = alpha;
    cnt = cnt + 1;

    % Calculate ERROR OF LINEAR REGRESSION
    out(cnt,j) = err;
    cnt = cnt + 1;

    % Calculate SHANNON ENTROPY
    out(cnt,j) = entropy(vec);
    cnt = cnt + 1;

    % Calculate 2nd ORDER RENYI ENTROPY
    out(cnt,j) = renyi_entropy(vec, 2);
    cnt = cnt + 1;

    % Calculate MODULATION        
    out(cnt,j) = data_range/(data_max + data_min);
    cnt = cnt + 1;

    % Calculate FIRST CORRELATION COEFFICIENT
    out(cnt,j) = corr_coef(vec(1:end-1), vec(2:end));
    cnt = cnt + 1;
end