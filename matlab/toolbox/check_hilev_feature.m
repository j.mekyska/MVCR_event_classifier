function pres = check_hilev_feature(hilev_desc, feature)

% pres = check_hilev_feature(hilev_desc, feature)
% 
% This function checks, if the particular high-level feature should be
% calculated or not.
% 
% hilev_desc    - cell array with a list of high-level features
% feature       - name of the feature
% pres          - 1 - feature should be calculated, 0 - otherwise

%% Check if the feature is checked
ind = find(strcmpi(hilev_desc(2:end,1),feature));

if(isempty(ind))
    error(['Feature ' feature ' is not present in the high-level ' ...
        'feature list.']);
end

if(length(ind) > 1)
    error(['Multiple occurancies of feature ' feature ' in the ' ...
        'high-level feature list.']);
end

ind = ind + 1;

pres = hilev_desc{ind,2};

%% If necessary, convert to number
if(ischar(pres))
    pres = str2double(pres);
end