function d = delta(coef, w)

% d = delta(coef, w)
% 
% This function returns the delta coefficients of input feature matrix.
% 
% coef  - input feature matrix where each segment corresponds to column
% w     - length of window` must be odd number (default: 9)
% d     - output matrix with delta coefficients

%% Paths and variables
if((nargin < 2) || isempty(w))
    w = 9;
end

%% Get the length of half of window
wover = fix(w/2);

%% Calculate the nominator
win = (-wover:1:wover);
win = win./sum(win.^2);

%% Filter the matrix
d = [repmat(coef(:,1),1,wover) coef repmat(coef(:,end),1,wover)];
d = filter(win, 1, d.').';
d = d(:,wover+1:end-wover);