function A = my_lpc_mtx(Y,p)

% A = my_lpc_mtx(Y,p)
% 
% This function returns a matrix of LPC coefficients.
% 
% Y     - input matrix (the LPC coefficients will be calculated for each
%         column)
% p     - order of LPC
% 
% A     - output matrix with LPC coefficients, if the input variable is a
%         matrix, then each row in matrix A corresponds to a column in
%         matrix Y

%% Check the input format
if(isrow(Y))
    Y = Y.';
end

%% Get the LPC coefficients
A = zeros(size(Y,2),p+1);

for i = 1:size(Y,2) % Over all columns
    A(i,:) = lpcauto(Y(:,i),p);
end