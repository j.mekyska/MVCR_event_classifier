function H = entropy(y, prec)

% H = entropy(y, prec)
% 
% This function calculates the Shannon entropy of input vector.
% 
% y     - input column vector
% prec  - precision of calculation; if prec = -1 then the precision will be
%         maximal (default: depending on the length of vector)
% H     - output entropy

%% Paths and variables
if((nargin < 2) || isempty(prec))
    if(length(y) < 64)
        prec = 32;
    elseif(length(y) < 128)
        prec = 64;
    else
        prec = 256;
    end
end

%% Calculate the probability
if(prec > 0)
    p = hist(y, prec);
    p = p./sum(p);
    p = p(p > 0);
else
    [tmp1, tmp2, n] = unique(y);
    
    p = hist(n,max(n));
    p = p./length(y);
end

%% Calculate the entropy
H = -sum(p.*log2(p));