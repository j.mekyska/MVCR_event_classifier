function coef = lpc2cep(lpc, p)

% lpc2cep(lpc, p)
% 
% This function transform LPC coefficients to Linear Predictive Cepstral
% Coefficients LPCC.
% 
% lpc   - matrix with LPC coefficients, segments are related to the columns
% p     - order od LPCC coefficients (default: same as LPC)
% coef  - matrix with LPCC coefficients, segments are related to the
%         columns

%% Paths and variables
if((nargin < 2) || isempty(p))
    p = size(lpc,1)-1;
end

%% Discart coefficient a[0]
lpc = lpc(2:end,:);

%% Calculate the LPCC coefficients
coef = zeros(p, size(lpc,2));
coef(1,:) = -lpc(1,:);

ind_stp = p;
if(p > size(lpc,1))
    ind_stp = size(lpc,1);
end

for h = 2:ind_stp
    tmp = zeros(1,size(lpc,2));
    
    for i = 1:h-1
        tmp = tmp + (i/h).*coef(i,:).*lpc(h-i,:);
    end
    
    coef(h,:) = -lpc(h,:) - tmp;
end

if(p > size(lpc,1))
    for h = (ind_stp+1):p
        tmp = zeros(1,size(lpc,2));
        
        for i = 1:ind_stp
            tmp = tmp + ((h-i)/h).*coef(h-i,:).*lpc(i,:);
        end
        
        coef(h,:) = -tmp;
    end
end