function m = my_trimmean(X, percent)

% my_trimmean(X, percent)
% 
% This function calculates the trimmed mean of the values in X. For a
% vector input, m is the mean of X, excluding the highest and lowest k data
% values, where k=n*(percent/100)/2 and where n is the number of values in
% X. For a matrix input, m is a row vector containing the trimmed mean of
% each column of X.
% 
% X         - input matrix or vector
% percent   - a scalar between 0 and 100
% 
% m         - output trimmed mean

%% Check the input format
if((percent < 0) || (percent >= 100))
    error('Variable percent must be a scalar between 0 and 100');
end

if(isrow(X))
    X = X.';
end

%% Get the LPC coefficients
X = sort(X);
n = size(X,1);
k = round(n*(percent/100)/2);

m = mean(X(k+1:end-k,:));