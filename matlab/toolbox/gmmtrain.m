function obj = gmmtrain(data, labels, nmix, iter, covmtx)

% obj = gmmtrain(data, labels, nmix, iter, covmtx)
% 
% This function train the GMM models for input multi-class data.
% 
% data      - source data where each column corresponds to feature and each
%             row corresponds to person
% labels    - column vector with labels (must be numbers)
% nmix      - number of mixtures in each model (default: 1)
% iter      - number of training iterations (default: 50)
% covmtx    - type of covariance matrix {'spherical', 'diag', 'full',
%             'ppca'} (default: 'diag')
% obj       - structure with the trained GMM models

%% Paths and variables
if((nargin < 3) || isempty(nmix))
    nmix = 1;
end
if((nargin < 4) || isempty(iter))
    iter = 50;
end
if((nargin < 5) || isempty(covmtx))
    covmtx = 'diag';
end

%% Train the models
ulabels = unique(labels);
obj = struct([]);

for i = 1:length(ulabels) % Over all labels
    train_data = data(labels == ulabels(i),:);
    
    obj(i).model = creategmmmodel(train_data, iter, nmix, covmtx);
    obj(i).label = ulabels(i);
end