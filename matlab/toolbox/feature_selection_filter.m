function selected = feature_selection_filter(data, labels, num_sel, method)

% selected = feature_selection_filter(data, labels, num_sel, method)
% 
% This function select the significant features according to the filtering
% methods. There are supported different methods (computational burden is
% in an ascending order): Conditional Mutual Info Maximisation,
% Min-Redundancy Max-Relevance, Joint Mutual Information, Double Input
% Symmetrical Relevance, Conditional Redundancy.
% 
% data      - input data matrix; columns are related to the features, rows
%             to the observations
% labels    - column vector with numeric labels of the observations (number
%             of rows must be eaqual to the number of rows in data)
% num_sel   - number of features to be selected
% method    - feature selection method {'cmim', 'mrmr', 'cmim', 'disr',
%             'condred'} (default: 'cmim')
% 
% selected  - column vector with selected IDs
% 
% Implemented according to
% Brown, G. and Pocock, A. and Zhao, M.J. and Luj{\'a}n, M.. Conditional
% Likelihood Maximisation: A Unifying Framework for Information Theoretic
% Feature Selection . In Journal of Machine Learning Research, (13) 27--66,
% Year 2012.

%% Paths and variables
if(~iscolumn(labels))
    error('Input variable ''labels'' must be a column vector.');
end

if(isrow(data))
    error(['Input variable ''data'' is a row vector. There must be ' ...
        'more lines to be able to split the matrix.']);
end

if(length(labels) ~= size(data,1))
    error(['Variables ''data'' and ''labels'' must have the same ' ...
        'number of rows.']);
end

if(size(data,2) < num_sel)
    error(['Number of features to be selected is ' num2str(num_sel) ...
        '. Number of features in data matrix is ' num2str(size(data,2)) ...
        '. Decrease the variable ''num_sel'' or increase the number of '...
        'features in matrix ''data''.']);
end

if(num_sel < 1)
    error(['Variable ''num_sel'' must be greather than 0.']);
end

if((nargin < 4) || isempty(method))
    method = 'cmim';
end

%% Select the features
switch(method)
    case 'cmim'
        selected = feast('cmim',num_sel,data,labels);
    case 'mrmr'
        selected = feast('mrmr',num_sel,data,labels);
    case 'jmi'
        selected = feast('cmim',num_sel,data,labels);
    case 'disr'
        selected = feast('disr',num_sel,data,labels);
    case 'condred'
        selected = feast('condred',num_sel,data,labels);
    otherwise
        error(['Unknow feature selection method. Please check the ' ...
            'supported methods.']);
end