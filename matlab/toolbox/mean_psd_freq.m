function f_mean = mean_psd_freq(y, fs, winlen, winover, padd)

% f_mean = mean_med_psd_freq(y, fs, winlen, winover, padd)
% 
% This function calculates the mean frequency of power spectrum.
% 
% y         - input column vector
% fs        - sampling frequency
% winlen    - length of window in samples or window samples
% winover   - window overlapp in samples
% padd      - if set to 1, the last segment will be padded by zeros if
%             necessary (default: 0)
% f_mean    - output mean frequency of power spectrum

%% Paths and variables
if((nargin < 5) || isempty(padd))
    padd = 0;
end

%% Segment the signal
Y = segmentation(y, winlen, winover, padd);

%% Calculate the power spectrum and integral
psd = abs(fft(Y)).^2;
psd = psd(1:fix(end/2),:);

int = sum(psd);
tmp = cumsum(psd)./int(ones(size(psd,1),1),:);

%% Calculate the mean frequency of power spectrum
f_mean = sum(tmp <= 0.5);
f_mean = (f_mean.*fs)./(size(psd,1)*2);