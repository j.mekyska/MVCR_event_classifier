function M = linbank(fs, n, p, eq)

% M = linbank(fs, n, p, eq)
% 
% This function returns the bank of triangular filters equally distributed
% in hertz scale.
% 
% fs    - sampling frequency
% n     - number of samples per filter
% p     - number of filters in bank (default: depending on fs)
% eq    - if set to 1 the equla loudness curves will be applied (default:
%         0)
% M     - output matrix, each row is corresponding to particular filter

%% Paths and variables
if((nargin < 3) || isempty(p))
    if(fs <= 8e3)
        p = 15;
    elseif(fs <= 11e3)
        p = 17;
    elseif(fs <= 16e3)
        p = 20;
    elseif(fs <= 22e3)
        p = 22;
    else
        p = 27;
    end
end
if((nargin < 4) || isempty(eq))
    eq = 0;
end

%% Calculate the center frequencies
c_f = zeros(3,p);
c_f(2,:) = (1:p).*(fix(fs/2)/(p+1));
c_f(1,:) = c_f(2,:)-c_f(2,1);
c_f(3,:) = c_f(2,:)+c_f(2,1);

%% Get the filter bank
M = zeros(p,n);
f = linspace(0,fix(fs/2),n);

for i = 1:p
    ind1 = sum(f < c_f(1,i))+1;
    ind2 = sum(f <= c_f(2,i));
    
    M(i,ind1:ind2) = (f(ind1:ind2)-c_f(1,i))./(c_f(2,i)-c_f(1,i));
    
    ind1 = sum(f <= c_f(2,i));
    ind2 = sum(f <= c_f(3,i));
    
    M(i,ind1:ind2) = (f(ind1:ind2)-c_f(3,i))./(c_f(2,i)-c_f(3,i));
end

%% Apply equal loudness curves if necessary
if(eq)
    E = eql_curves(fs, n);
    
    tmp = c_f(3,:)';
    tmp = f(ones(p,1),:) <= tmp(:,ones(1,n));
    
    ind = sum(tmp.');    
    m = E(ind).';
    m = m(:,ones(1,n));
    
    M = M.*m;
end