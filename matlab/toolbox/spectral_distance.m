function [Dm, Dp] = spectral_distance(y, wind, winover, padd, NFFT)

% [Dm, Dp] = spectral_distance(y, wind, winover, padd, NFFT)
% 
% This function calculates the spectral distances based on module and
% phase.
% 
% y         - input column vector
% wind      - length of window in samples or window samples
% winover   - window overlapp in samples
% padd      - if set to 1, the last segment will be padded by zeros if
%             necessary (default: 0)
% NFFT      - number of FFT points (default: 2048)
% Dm        - column vector with spectral distance based on module
% Dp        - column vector with spectral distance based on phase
% 
% Implemented according to
% J.B. Alonso, J. de Leo�n, I. Alonso, M.A. Ferrer, Automatic detection of
% pathologies in the voice by HOS based parameters, EURASIP J. Appl. Signal
% Process. 2001 (4) (2001) 275�284.
% 
% Comments
% Dm increases with increasing noise

%% Paths and variables
if((nargin < 4) || isempty(padd))
    padd = 0;
end
if((nargin < 5) || isempty(NFFT))
    NFFT = 2048;
end

%% Segment the input signal
Y = segmentation(y, wind, winover, padd);

%% Calculate the spectrum
S = fft(Y, NFFT);

%% Calculate the distance based on module and phase
Dm = sum(abs(abs(S(:,1:end-1)) - abs(S(:,2:end)))).';
Dp = sum(abs(abs(angle(S(:,1:end-1))) - abs(angle(S(:,2:end))))).';