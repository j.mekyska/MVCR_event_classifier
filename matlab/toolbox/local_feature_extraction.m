function res = local_feature_extraction(y, fs, desc)

% res = local_feature_extraction(y, fs, desc)
% 
% This function performs parametrization of 1D signal and returns the
% extracted features in structure described below.
% 
% y     - column vector containing the signal
% fs	- sampling frequency
% desc  - determines whether to get feature description as well
% 
% res	- output structure

%% Paths and variables
no_of_features = 9900;

sett.win_len = 0.100; % Length of window
sett.win_over = 0.050; % Window overlap
sett.NFFT = 1024; % Number of FFT points
sett.p = 16; % Number of filters in the MFCC/LFCC filter bank (must be the power of two)
% sett.LPC_p = fix(fs/1e3)+2; % Order of linear prediction
sett.LPC_p = 10; % Order of linear prediction
sett.SF_per = 0.500; % Period (in seconds) of longitudial frame

%% Prepare the output structure
res.data = zeros(no_of_features, 1);
cnt = 1;

if(desc)
    [~, hilev_names] = hilev_feature_extraction(zeros(10,1));
end

%% Median frequency of power spectrum
data_PSD_loc = mean_psd_freq(y, fs, hamming(fs*sett.win_len), ...
    fs*sett.win_over, 0);
data_PSD_hi = hilev_feature_extraction(data_PSD_loc.');

for i = 1:size(data_PSD_hi,2) % Over all segments
    for j = 1:size(data_PSD_hi,1) % Over all features
        res.data(cnt) = data_PSD_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' ...
                'median frequency of power spectrum'];
        end
        
        cnt = cnt + 1;
    end
end

%% Short-time energy
data_E_loc = energy(y, hamming(fs*sett.win_len), fs*sett.win_over, 0);
data_E_hi = hilev_feature_extraction(data_E_loc.');

for i = 1:size(data_E_hi,2) % Over all segments
    for j = 1:size(data_E_hi,1) % Over all features
        res.data(cnt) = data_E_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' ...
                'short-time energy'];
        end
        
        cnt = cnt + 1;
    end
end

%% Teager Kaiser Energy operator
data_TEO_loc = teo(y);
data_TEO_hi = hilev_feature_extraction(data_TEO_loc);

for i = 1:size(data_TEO_hi,2) % Over all segments
    for j = 1:size(data_TEO_hi,1) % Over all features
        res.data(cnt) = data_TEO_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' ...
                'Teager Kaiser operator'];
        end
        
        cnt = cnt + 1;
    end
end

%% Spectral flux
data_SF_loc = spectrum_flux(y, fs, hamming(fs*sett.win_len), ...
        fs*sett.win_over, 0, sett.SF_per);
data_SF_hi = hilev_feature_extraction(data_SF_loc.');

for i = 1:size(data_SF_hi,2) % Over all segments
    for j = 1:size(data_SF_hi,1) % Over all features
        res.data(cnt) = data_SF_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' ...
                'spectral flux'];
        end
        
        cnt = cnt + 1;
    end
end

%% Spectral distance
[data_Dm_loc, data_Dp_loc] = spectral_distance(y, ...
    hamming(fs*sett.win_len), fs*sett.win_over, 0, sett.NFFT);

data_Dm_hi = hilev_feature_extraction(data_Dm_loc);
data_Dp_hi = hilev_feature_extraction(data_Dp_loc);

for i = 1:size(data_Dm_hi,2) % Over all segments
    for j = 1:size(data_Dm_hi,1) % Over all features
        res.data(cnt) = data_Dm_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' ...
                'spectral distance based on module'];
        end
        
        cnt = cnt + 1;
    end
end

for i = 1:size(data_Dp_hi,2) % Over all segments
    for j = 1:size(data_Dp_hi,1) % Over all features
        res.data(cnt) = data_Dp_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' ...
                'spectral distance based on phase'];
        end
        
        cnt = cnt + 1;
    end
end

%% Mel frequency cepstral coefficients
data_MFCC_loc = coeff_mfcc(y, fs, hamming(fs*sett.win_len), ...
    fs*sett.win_over, 0, sett.NFFT, sett.p, 0);
data_MFCC_delta_loc = delta(data_MFCC_loc, 9);
data_CMS_loc = zscore(data_MFCC_loc.').';
data_CMS_delta_loc = delta(data_CMS_loc, 9);

data_MFCC_hi = hilev_feature_extraction(data_MFCC_loc.');
data_MFCC_delta_hi = hilev_feature_extraction(data_MFCC_delta_loc.');
data_CMS_hi = hilev_feature_extraction(data_CMS_loc.');
data_CMS_delta_hi = hilev_feature_extraction(data_CMS_delta_loc.');

for i = 1:size(data_MFCC_hi,2) % Over all segments
    for j = 1:size(data_MFCC_hi,1) % Over all features
        res.data(cnt) = data_MFCC_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'mel frequency cepstral coeff.'];
        end
        
        cnt = cnt + 1;
    end
end

for i = 1:size(data_MFCC_delta_hi,2) % Over all segments
    for j = 1:size(data_MFCC_delta_hi,1) % Over all features
        res.data(cnt) = data_MFCC_delta_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'delta coeff. of mel frequency cepstral coefficients'];
        end
        
        cnt = cnt + 1;
    end
end

for i = 1:size(data_CMS_hi,2) % Over all segments
    for j = 1:size(data_CMS_hi,1) % Over all features
        res.data(cnt) = data_CMS_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'cepstral mean subtraction coeff.'];
        end
        
        cnt = cnt + 1;
    end
end

for i = 1:size(data_CMS_delta_hi,2) % Over all segments
    for j = 1:size(data_CMS_delta_hi,1) % Over all features
        res.data(cnt) = data_CMS_delta_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'delta coeff. of cepstral mean subtraction coeff.'];
        end
        
        cnt = cnt + 1;
    end
end

%% Linear frequency cepstral coefficients
data_LFCC_loc = coeff_lfcc(y, fs, hamming(fs*sett.win_len), ...
    fs*sett.win_over, 0, sett.NFFT, sett.p, 0);
data_LFCC_delta_loc = delta(data_LFCC_loc, 9);

data_LFCC_hi = hilev_feature_extraction(data_LFCC_loc.');
data_LFCC_delta_hi = hilev_feature_extraction(data_LFCC_delta_loc.');

for i = 1:size(data_LFCC_hi,2) % Over all segments
    for j = 1:size(data_LFCC_hi,1) % Over all features
        res.data(cnt) = data_LFCC_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'linear frequency cepstral coefficients'];
        end
        
        cnt = cnt + 1;
    end
end

for i = 1:size(data_LFCC_delta_hi,2) % Over all segments
    for j = 1:size(data_LFCC_delta_hi,1) % Over all features
        res.data(cnt) = data_LFCC_delta_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'delta coeff. of linear frequency cepstral coefficients'];
        end
        
        cnt = cnt + 1;
    end
end

%% Linear predictive coefficients
data_LPC_loc = coeff_lpc(y, fs, hamming(fs*sett.win_len), ...
        fs*sett.win_over, 0, sett.LPC_p);
data_LPC_delta_loc = delta(data_LPC_loc, 9);

data_LPC_hi = hilev_feature_extraction(data_LPC_loc.');
data_LPC_delta_hi = hilev_feature_extraction(data_LPC_delta_loc.');

for i = 1:size(data_LPC_hi,2) % Over all segments
    for j = 1:size(data_LPC_hi,1) % Over all features
        res.data(cnt) = data_LPC_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'linear predictive coefficients'];
        end
        
        cnt = cnt + 1;
    end
end

for i = 1:size(data_LPC_delta_hi,2) % Over all segments
    for j = 1:size(data_LPC_delta_hi,1) % Over all features
        res.data(cnt) = data_LPC_delta_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'delta coeff. of linear predictive coefficients'];
        end
        
        cnt = cnt + 1;
    end
end

%% Linear predictive cepstral coefficients
data_LPC_loc = coeff_lpc(y, fs, hamming(fs*sett.win_len), ...
        fs*sett.win_over, 0, sett.LPC_p);
data_LPCC_loc = lpc2cep(data_LPC_loc);
data_LPCC_delta_loc = delta(data_LPCC_loc, 9);

data_LPCC_hi = hilev_feature_extraction(data_LPCC_loc.');
data_LPCC_delta_hi = hilev_feature_extraction(data_LPCC_delta_loc.');

for i = 1:size(data_LPCC_hi,2) % Over all segments
    for j = 1:size(data_LPCC_hi,1) % Over all features
        res.data(cnt) = data_LPCC_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'linear predictive cepstral coefficients'];
        end
        
        cnt = cnt + 1;
    end
end

for i = 1:size(data_LPCC_delta_hi,2) % Over all segments
    for j = 1:size(data_LPCC_delta_hi,1) % Over all features
        res.data(cnt) = data_LPCC_delta_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'delta coeff. of linear predictive cepstral coefficients'];
        end
        
        cnt = cnt + 1;
    end
end

%% Adaptive component weighted cepstral coefficients
data_LPC_loc = coeff_lpc(y, fs, hamming(fs*sett.win_len), ...
        fs*sett.win_over, 0, sett.LPC_p);
data_ACW_loc = coeff_acw(data_LPC_loc);
data_ACW_delta_loc = delta(data_ACW_loc, 9);

data_ACW_hi = hilev_feature_extraction(data_ACW_loc.');
data_ACW_delta_hi = hilev_feature_extraction(data_ACW_delta_loc.');

for i = 1:size(data_ACW_hi,2) % Over all segments
    for j = 1:size(data_ACW_hi,1) % Over all features
        res.data(cnt) = data_ACW_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'adaptive component weighted cepstral coefficients'];
        end
        
        cnt = cnt + 1;
    end
end

for i = 1:size(data_ACW_delta_hi,2) % Over all segments
    for j = 1:size(data_ACW_delta_hi,1) % Over all features
        res.data(cnt) = data_ACW_delta_hi(j,i);
        
        if(desc)
            res.desc{cnt} = [hilev_names{j} ' of ' num2str(i) '. '  ...
                'delta coeff. of adaptive component weighted cepstral ' ...
                'coefficients'];
        end
        
        cnt = cnt + 1;
    end
end

%% Check the final number of features
if(desc && ((cnt-1) ~= no_of_features))
    error(['Allocated number of features is ' num2str(no_of_features) ...
        ', however, the real number of features is ' ...
        num2str(cnt-1)]);
end