%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(genpath('toolbox'));

pth_data = ['..' filesep 'meta' filesep 'table_results_12k.csv'];

rng(1);
folds = 10;

threshold = 0.000246;

%% Get the feature indexes
[filepath, name] = fileparts(pth_data);
pth_features = fullfile(filepath, [name '_SFFS_GMM.xlsx']);

[~, ~, indexes] = xlsread(pth_features);
indexes = cell2mat(indexes(9:end,2)).';

%% Prepare the data and labels
% Load the *.csv
table = importdata(pth_data,',',1);

% Filter out the neutral events
sel = cell2mat(cellfun(@(x) strcmp(x,'neutral'),table.textdata(:,4), ...
    'UniformOutput',false)) == 0;
table.textdata = table.textdata(sel,:);
table.data = table.data(sel(2:end),:);

% Prepare the labels
labels = cell2mat(cellfun(@(x) strcmp(x,'train') | strcmp(x,'train2') ...
    | strcmp(x,'cargoTrain'),table.textdata(2:end,4), ...
    'UniformOutput',false));
labels = double(labels);

% Prepare and filter the data
data = table.data;
data = data(:,indexes);

%% Train the model
sel = labels == 1;

tmp_data = data(sel,:);
tmp_labels = labels(sel,:);

% Get the 10-fold indexes
ind = crossvalind('Kfold', size(tmp_data,1), folds);

data_train = tmp_data(ind ~= 1,:);
labels_train = tmp_labels(ind ~= 1,:);
data_test = tmp_data(ind == 1,:);
labels_test = tmp_labels(ind == 1,:);

% Train GMM
obj = gmmtrain(data_train, labels_train, 1, 50, 'full');

%% Test the GMM
% prob_train = gmmprob(obj.model, data_test);
prob_train = gmmprob(obj.model, data(labels == 1,:));
prob_other = gmmprob(obj.model, data(labels == 0,:));

figure;
histogram(prob_train, 50);
hold on;
histogram(prob_other, 50);
plot(threshold,0,'r*');
hold off;
axis tight;
legend('train','neutral','threshold');

%% Store the model
obj.model.threshold = threshold;
obj.model.feature_indexes = indexes;

model = obj.model;

[filepath, name] = fileparts(pth_data);
pth_out = fullfile(filepath, [name '_GMM_model.mat']);

disp(['Saving the final GMM model to' pth_out]);
save(pth_out,'model');