%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(genpath(['..' filesep '..' filesep 'matlab' filesep 'toolbox']));

frame = struct([]);
load('test.mat');

y = frame.data;
fs = frame.fs;

dest_fs = 1000;
num_of_samples = 997;

%% Decimate     
if(fs ~= dest_fs)
    G = gcd(fs, dest_fs);
    Q = fs/G;
    P = dest_fs/G;

    y = resample(y, P, Q);

    fs = dest_fs;
end

if(length(y) > num_of_samples)
    disp(['Trimming the length of signal from ' ...
        num2str(length(y)) ' to ' num2str(num_of_samples)]);
    y = y(1:num_of_samples);
end

%% Run parameterisation
S = get_spectrogram(y);