function S = get_spectrogram(y)

% S = get_spectrogram(y)
% 
% This function returns a row vector of spectrogram of the input column
% vector containing raw data.
% 
% y         - input column vector with raw data
% 
% S         - output row vector with features
% 
% WARNING
% 1) Variable 'no_of_features' must have the same value as in
% local_feature_extraction.m.

%% Paths and variables
winlen = 0.020;
winover = 0.010;
NFFT = 1000;
fs = 1000;

%% Calculate the spectrogram
% Normalize
y = y./max(abs(y));

% Subtract mean
y = y-mean(y);

% Calculate the spectrogram
S = my_spectrogram(y,hamming(fix(fs*winlen)),fix(winover*fs),NFFT);
S = 20*log10(abs(S));
S = reshape(S,1,[]);