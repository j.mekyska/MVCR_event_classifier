function F = get_feature_matrix(M)

% F = get_feature_matrix(M)
% 
% This function returns a row vector of fetaures of the input column vector
% containing raw data.
% 
% M         - input column vector with raw data
% 
% F         - output row vector with features
% 
% WARNING
% 1) Variable 'no_of_features' must have the same value as in
% local_feature_extraction.m.

%% Paths and variables
fs = 1e3;
no_of_features = 9900;
desc = 0;

%% Extract the features
F = zeros(size(M,2), no_of_features);

for i = 1:size(M,2) % Over all signals in the input matrix
    y = M(:,i);
    
    % Normalize
    y = y./max(abs(y));

    % Subtract mean
    y = y-mean(y);
    
    res = local_feature_extraction(y, fs, desc);
    F(i,:) = res.data;
end