function out = get_feature_vector(y, fs, sel_features, local_feat, hilev_feat)

% out = get_feature_vector(y, fs, sel_features, local_feat, hilev_feat)
% 
% This function returns a vector of features calculated for one column
% signal.
% 
% y             - input signal in column vector
% fs            - sampling frequency
% sel_features  - list of features to be selected
% local_feat    - list of local features that should be calculated
% hilev_feat    - list of hilev features that should be calculated
% 
% out           - output column vector with features

%% Normalize
y = y./max(abs(y));

%% Extract the local features
features = local_feature_extraction(y, fs, local_feat);

%% Extract the high-level features
features = hilev_feature_extraction(features, hilev_feat);

%% Select specific features
out = zeros(size(sel_features,1),2);

for i = 1:size(features,2) % Over all features
    ind = [];

    for j = 1:size(sel_features,1)
        if(strcmp(sel_features{j}, features(i).desc))
            ind = j;
            break;
        end
    end
    
    if(~isempty(ind))
        out(ind,1) = features(i).data;
        out(ind,2) = 1;
    else
        continue;
    end
end

% Check whether all features were calculated
sel = find(out(:,2) == 0,1,'first');

if(~isempty(sel))
    error(['Feature ''' sel_features{sel} ''' is not present in ' ...
        'the feature set that is calculated']);
else
    out = out(:,1);
end