%% Initial clean up
close all;
clear all;
clc;

%% Paths and variables
addpath(genpath(['..' filesep '..' filesep 'matlab' filesep 'toolbox']));

frame = struct([]);
load('test.mat');

y = frame.data;
fs = frame.fs;

dest_fs = 1000;

%% Decimate     
if(fs ~= dest_fs)
    G = gcd(fs, dest_fs);
    Q = fs/G;
    P = dest_fs/G;

    y = resample(y, P, Q);

    fs = dest_fs;
end

%% Prepare a matrix
M = repmat(y,1,1);

%% Run parameterisation
F = get_feature_matrix(M);